import torch
from torchtext.data.utils import get_tokenizer
import numpy as np


def accuracy(output, label, threshold=0.1):
    '''
    Given the output predictions and labels, caluclaes model accuracy
    '''
    correct = [1 if x < threshold and y == 0 or x >= threshold and y == 1 else 0 for (x, y) in zip(output, label)]
    acc = np.array(correct).sum() / len(correct)

    return acc


def train(model, iterator, optimizer, criterion):
    '''
    Training a model
    '''

    # initialize every epoch
    epoch_loss = 0
    epoch_acc = 0

    # set the model in training phase
    model.train()

    for batch in iterator:
        # resets the gradients after every batch
        model.zero_grad()

        # retrieve text and no. of words
        text1, _ = batch.code1
        text2, _ = batch.code2

        # convert to 1D tensor
        output = model(text1, text2)
        output = output.flatten()

        # compute the accuracy
        acc = accuracy(output, batch.label)

        # compute the loss
        loss = criterion(output, batch.label)

        # backpropage the loss and compute the gradients
        loss.backward()

        # update the weights
        optimizer.step()

        # loss and accuracy
        epoch_loss += loss.item()
        epoch_acc += acc.item()

    return epoch_loss / len(iterator), epoch_acc / len(iterator)


def evaluate(model, iterator, criterion):
    '''
    Evaluating a model
    '''
    # initialize every epoch
    epoch_loss = 0
    epoch_acc = 0

    # deactivating dropout layers
    model.eval()

    # deactivates autograd
    with torch.no_grad():
        for batch in iterator:
            # retrieve text and no. of words
            text1, _ = batch.code1
            text2, _ = batch.code2

            # convert to 1d tensor
            output = model(text1, text2)
            output = output.flatten()

            # compute the accuracy
            acc = accuracy(output, batch.label)
            loss = criterion(output, batch.label)

            # keep track of loss and accuracy
            epoch_loss += loss.item()
            epoch_acc += acc.item()

    return epoch_loss / len(iterator), epoch_acc / len(iterator)


def predict(model, first_code, second_code):
    '''
    Predicts the output of the model for a single pair
    '''
    model.eval()
    with torch.no_grad():
        tokenize = get_tokenizer("moses")
        first_code_tensor = string_code_to_tensor(first_code, tokenize)
        second_code_tensor = string_code_to_tensor(second_code, tokenize)

        # prediction
        output = model(first_code_tensor, second_code_tensor)
        print(output)
        output = output.flatten()

        print("Predicted output: " + str(output))


def string_code_to_tensor(code, tokenize):
    '''
    Preprocesses a single code given as string into tensor
    so that it can be passed to the model
    '''
    # tokenize the code
    code_tokenized = [token for token in tokenize(code)]

    # tokens to index in embedding matrix
    code_indexed = [TEXT.vocab.stoi[token] for token in code_tokenized]

    # converting to tensor
    code_tensor = torch.LongTensor(code_indexed).to(device)

    # reshape the tensor
    code_tensor = code_tensor.unsqueeze(1).T

    return code_tensor
