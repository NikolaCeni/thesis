import torch.nn as nn
import torch
import torch.nn.functional as F


class Classifier(nn.Module):
    '''
    The class where the model architecture is implemented
    '''

    # define all the layers used in model
    def __init__(self, vocab_size, embedding_dim, hidden_dim, output_dim, n_layers, padding_length,
                 bidirectional, dropout):
        # Constructor
        super().__init__()
        self.embedding = nn.Embedding(vocab_size, embedding_dim)

        # lstm layer
        self.lstm = nn.LSTM(embedding_dim,
                            hidden_dim,
                            num_layers=n_layers,
                            bidirectional=bidirectional,
                            dropout=dropout,
                            batch_first=True)

        # dense layer
        self.fc = nn.Linear(hidden_dim, output_dim)
        self.dense_layer = nn.Linear(output_dim * 2, 16)
        self.dense_layer2 = nn.Linear(16, 1)

    def forward_once(self, text):
        '''
        Used for processing a single code
        '''
        embedded = self.embedding(text)
        packed_output, (hidden, cell) = self.lstm(embedded)
        hidden = hidden[-1, :, :]
        dense_outputs = F.relu(self.fc(hidden))
        return dense_outputs

    def forward(self, code1, code2):
        '''
        Sending the preprocessed code1 and code2 through the rest of the network
        '''
        # forward pass of input 1
        output1 = self.forward_once(code1)
        # forward pass of input 2
        output2 = self.forward_once(code2)
        x = torch.cat((output1, output2), dim=1)
        x = F.relu(self.dense_layer(x))
        x = self.dense_layer2(x)
        label = torch.sigmoid(x)
        return label
