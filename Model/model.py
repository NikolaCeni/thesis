import warnings
from Editor.AuxFiles.auxFunctions import is_empty_or_new_line
from torchtext.data.utils import get_tokenizer
from torchtext import data
from torchtext.vocab import Vectors
from Model.Classifier import *
from Replacer.variablesReplacer import VariablesReplacer

'''
Module which loads the vector embeddings, instantiates the model and initilizes the pretrained model weights
Contains the predict function which gives the prediction of the model
'''


class Model:
    def __init__(self, editor_path):
        print('------------------------------------')
        print("Loading the model...")
        # ignores the warnings produced by retiring some pytorch classes
        warnings.filterwarnings('ignore')

        self.tokenizer = get_tokenizer("moses")
        self.define_paths(editor_path)
        self.create_data_fields()
        self.load_dataset()
        self.load_embeddings()
        self.build_vocabulary()
        self.create_model()
        self.load_model()
        self.initialize_embeddings()
        print('Model loaded.')
        print('------------------------------------')

    def define_paths(self, editor_path):
        '''
        Defines the editor, data and saved weights paths
        '''
        self.editor_path = editor_path
        self.data_file_path = self.editor_path + 'Model\\Data_Weights\\10.03\\new_data_10_03.csv'
        self.saved_weights_path = self.editor_path + 'Model\\Data_Weights\\10.03\\saved_weights_new_data_10_03.pt'

    def create_data_fields(self):
        '''
        Creating fields used for data loading
        '''
        self.TEXT = data.Field(tokenize=self.tokenizer, batch_first=True, include_lengths=True)
        self.LABEL = data.LabelField(dtype=torch.float, batch_first=True)
        self.fields = [('code1', self.TEXT), ('code2', self.TEXT), ('label', self.LABEL)]

    def load_dataset(self):
        '''
        Loading custom dataset from csv file
        '''
        self.training_data = data.TabularDataset(path=self.data_file_path, format='csv', fields=self.fields,
                                                 skip_header=True)

    def load_embeddings(self):
        '''
        Loading pretrained vector embeddings
        '''
        self.vectors = Vectors(name="w2v_freq_1_10_03.txt", cache=self.editor_path + 'Model\\embedding\\10.03')
        self.min_freq_for_embedding = 1

    def build_vocabulary(self):
        '''
        Initialize embeddings
        '''
        self.TEXT.build_vocab(self.training_data, min_freq=self.min_freq_for_embedding, vectors=self.vectors)
        self.TEXT.vocab.set_vectors(self.vectors.stoi, self.vectors.vectors, self.vectors.dim)

    def create_model(self):
        '''
        Instanciates the model
        '''
        # setting hyperparameters
        size_of_vocab = len(self.TEXT.vocab)
        embedding_dim = 100
        num_hidden_nodes = 32
        num_output_nodes = 16
        num_layers = 2
        bidirection = True
        dropout = 0.2
        padding_length = 250

        # instantiate the model
        self.model = Classifier(size_of_vocab, embedding_dim, num_hidden_nodes, num_output_nodes, num_layers,
                                padding_length,
                                bidirectional=bidirection, dropout=dropout)

    def load_model(self):
        '''
        Loads the pretrained model weights
        '''
        self.model.load_state_dict(torch.load(self.saved_weights_path, map_location=torch.device('cpu')))
        self.model.eval()

    def initialize_embeddings(self):
        '''
        Initialize the pretrained embedding
        '''
        pretrained_embeddings = self.TEXT.vocab.vectors
        self.model.embedding.weight.data.copy_(pretrained_embeddings)

    def predictAux(self, first_code, second_code, threshold=0.99, preprocessor=None):
        '''
        Preprocess the first_code and second_code and gives back the model prediction
        '''
        if (is_empty_or_new_line(first_code) or is_empty_or_new_line(second_code)):
            return (False, 1)
        else:
            first_code_tokenized = preprocessor.preprocess_single_code(first_code)
            second_code_tokenized = preprocessor.preprocess_single_code(second_code)

            first_code_indexed = [self.TEXT.vocab.stoi[t] for t in first_code_tokenized]  # convert to integer sequence
            second_code_indexed = [self.TEXT.vocab.stoi[t] for t in second_code_tokenized]

            first_code_tensor = torch.LongTensor(first_code_indexed)  # convert to tensor
            second_code_tensor = torch.LongTensor(second_code_indexed)

            first_code_tensor = first_code_tensor.unsqueeze(1).T  # reshape in form of batch,no. of words
            second_code_tensor = second_code_tensor.unsqueeze(1).T

            output = self.model(first_code_tensor, second_code_tensor)  # prediction
            output = output.tolist()[0][0]
            predicted_value_limit = 1 - threshold
            if output <= predicted_value_limit:
                return (True, output)
            return (False, output)

    def predict(self, code1, code2, threshold=0.95, preprocessor=None):
        '''
        Preprocess the first_code and second_code and gives back the model prediction
        '''
        if (preprocessor == None):
            preprocessor = VariablesReplacer()
        return self.predictAux(code1, code2, threshold, preprocessor)
