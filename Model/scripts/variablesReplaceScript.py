import random
import string
import re
'''
Initial method of replacing variables, functions and class names.
Later on replced by the VariablecReplacer class
'''
def removeComments(filename):
    newFileName=filename
    f=open(filename,"r")
    code=""
    for i in f.readlines():
        code+=i.split('#')[0]
        if(i.split('#')[0]!='\n'):
            code+='\n'
    
    lines = code.split("\n")
    non_empty_lines = [line for line in lines if line.strip() != ""]
    string_without_empty_lines = ""
    for line in non_empty_lines:
      string_without_empty_lines += line + "\n"   
    f=open(newFileName,"w")
    f.write(string_without_empty_lines)
    
def generateClones(filename,variablesList,classesList,functionsList,numbersList,stringsList,numberOfClones):
    removeComments(filename)
    for i in range(0,numberOfClones+1):
        newFileName=filename[0:-3]+str(i)+".py"
        f=open(filename,"r")
        code=f.read()
        f.close()       
        for num in range(0,len(numbersList)):
            code = re.sub(r'\b' + numbersList[num] + r'\b', str(random.randint(-100,100)), code)
        for var in range(0,len(variablesList)):
            code = re.sub(r'\b' + variablesList[var] + r'\b', 'var'+str(len(variablesList)*i+var), code)
        for cl in range(0,len(classesList)):
            code = re.sub(r'\b' + classesList[cl] + r'\b', 'class'+str(len(classesList)*i+cl), code)
        for fun in range(0,len(functionsList)):
            code = re.sub(r'\b' + functionsList[fun] + r'\b', 'fun'+str(len(functionsList)*i+fun), code)
        for st in range(0,len(stringsList)):
            code = re.sub(r'\b' + stringsList[st] + r'\b', 'string'+str(len(stringsList)*i+st), code)
        f=open(newFileName,"w")
        f.write(code)


###Explanation: replaces variables with var[0..] classes with class[0..] functions with fun[0..]  numbers with random num from -100 to 100 and other strings with string[0..]
###How to call the function:
###generateClones(FILENAME,LIS OF VARIABLES,LIST OF CLASSES,LIST OF FUNCTIONS, LIST OF NUMBERS AS STRINGS,LIST OF OTHER STRINGS )

# generateClones("../Data/GeneratedData/0/zero.py",['kj','l'],[],[],['5','4','1','10'],[],50)
# generateClones("../Data/GeneratedData/1/first.py",['nik','d'],[],[],['3','4'],[],50)
# generateClones("../Data/GeneratedData/2/second.py",['ff','dd','gg'],[],[],['2','5','1'],[],50)
# generateClones("../Data/GeneratedData/3/third.py",['ik'],[],[],['2','7','5'],[],50)
# generateClones("../Data/GeneratedData/4/fourth.py",['gas','num','upper','lower'],[],[],['900','1000','1','2','0'],[],50)
# generateClones("../Data/GeneratedData/5/fifth.py",['num','num_sqrt'],[],[],['8','1','2'],[],50)
# generateClones("../Data/GeneratedData/6/sixth.py",['num1','num2','num3','largest'],[],[],['10','14','12'],[],50)
# generateClones("../Data/GeneratedData/7/seventh.py",['num','gol'],[],[],['407','1','2','0'],[],50)
# generateClones("../Data/GeneratedData/8/eighth.py",['num'],[],[],['3434','2','0'],[],50)
# generateClones("../Data/GeneratedData/9/nineth.py",['celsius','fahrenheit'],[],[],['37','5','1','8','32'],[],50)
# generateClones("../Data/GeneratedData/10/tenth.py",['temp','x','y'],[],[],['5','10'],[],50)
# generateClones("../Data/GeneratedData/11/eleventh.py",['gol','num','factorial'],[],[],['7','1','0'],[],50)
# generateClones("../Data/GeneratedData/12/twelve.py",['num','gol'],[],[],['1'],[],50)
# generateClones("../Data/GeneratedData/13/thirthteen.py",['nterms','n1','n2','count'],[],[],['12','1','11'],[],50)
# generateClones("../Data/GeneratedData/14/fourteenth.py",['lower','upper','num','order','sum','digit','temp'],[],[],['100','2000','1','10','0'],[],50)
# generateClones("../Data/GeneratedData/15/fifthteen.py",['num','sum'],[],[],['16','0','1'],[],50)
# generateClones("../Data/GeneratedData/16/sixteenth.py",['cccc'],[],[],[],[],50)
# generateClones("../Data/GeneratedData/17/seventeenth.py",['result','x','my_list'],[],[],['0','13', '12' , '65' , '54' , '39' , '102' , '339' , '221' ],[],50)
# generateClones("../Data/GeneratedData/18/eightteenth.py",['num','x','print_factors'],[],[],['1'],[],50)
# generateClones("../Data/GeneratedData/19/nineteenth.py",['punctuations','my_str','no_punct','char'],[],[],[],["Hello", "he", "said" ,'and', "went"],50)
# generateClones("../Data/GeneratedData/20/twentieth.py",['X','Y','result','rog','result','px','jx'],[],[],['12','7','3','4','5','6','7','8','9','1','0'],[],50)
# generateClones("../Data/GeneratedData/21/twenyone.py",['x','y','greater'],[],['compute_lcm'],['0','54','24','1'],[],50)
# generateClones("../Data/GeneratedData/22/22th.py",['x','y'],[],['sth'],['1','100','23','32','34'],[],50)
# generateClones("../Data/GeneratedData/23/23th.py",['deck','nik'],[],[],['5','0','14','1'],[],50)
# generateClones("../Data/GeneratedData/24/24th.py",['nnn','kk','nterms'],[],['recur_fibo'],['1','0'],[],50)
# generateClones("../Data/GeneratedData/25/25th.py",['nnn','num'],[],['recur_sum'],['1','16'],[],50)
# generateClones("../Data/GeneratedData/26/26th.py",['nnn','num'],[],['recur_factorial'],['7','1'],[],50)
# generateClones("../Data/GeneratedData/27/27th.py",['nn','dec'],[],['convertToBinary'],['1'],[],50)
# generateClones("../Data/GeneratedData/28/28th.py",['my_str','rev_str'],[],[],[],['The','string','is a palindrome','reverse the string'],50)
# generateClones("../Data/GeneratedData/29/29th.py",['my_str','word'],[],[],[],["Hello",'this Is an','Example','The','sorted','words','are:'],50)
# generateClones("../Data/GeneratedData/30/30th.py",['N','E'],[],[],['2','3','4','5','1','6','8','0'],['Intersection','Difference','Symmetric'],50)
# generateClones("../Data/GeneratedData/31/31th.py",['vowels','ip_str','count','char'],[],[],['1'],['Hello,','have' ,'you tried' ,'our tutorial','section yet?','aeiou'],50)
# generateClones("../Data/GeneratedData/32/32th.py",['names_file','body_file','body','name','mail'],[],[],[],["Hello"],50)
# generateClones("../Data/GeneratedData/33/33th.py",['filename','img_file','a','height','width'],[],['jpeg_res'],['8','0','1','2'],["The", 'resolution' ,'of the', 'image is'],50)
# generateClones("../Data/GeneratedData/34/34th.py",['filename','chunk'],[],['hash_file'],[],['track1'],50)
# generateClones("../Data/GeneratedData/35/35th.py",['nn','ii','sum'],[],['sumOfSeries'],['0'],[],50)
# generateClones("../Data/GeneratedData/36/36th.py",['nn','ni','sm'],[],['squaresum'],['0','4'],[],50)
# generateClones("../Data/GeneratedData/37/37th.py",['sum','nn','ans','arr'],[],['_sum'],['12','3','4','15'],['Sum','of','the','array','is'],50)
# generateClones("../Data/GeneratedData/38/38th.py",['arr','id','ii','arr','temp','dd','nn'],[],['leftRotate','leftRotatebyOne','printArray'],['1', '2', '3', '4', '5', '6', '7'],[],50)
# generateClones("../Data/GeneratedData/39/39th.py",['arr','start','end','temp','id','nn','dd'],[],['printArray','leftRotate','rverseArray'],['1','2','3','4','5','6','7'],[],50)
# generateClones("../Data/GeneratedData/40/40th.py",['test_list','counter','ii'],[],[],['1', '4', '5', '7', '8','0'],['Length','of' ,'list','using naive method', 'is :','The',':'],50)

# generateClones("../Data/GeneratedData/41/41st.py",['test_list','i','using', 'loop','Element', 'Exists'],[],[],["1","6","3","5","4"],["Checking", "exists", "list", "( using loop )"],50)
# generateClones("../Data/GeneratedData/42/42th.py",['GEEK'],[],[],['6','0','4','1'],['before', 'hi'],50)
# generateClones("../Data/GeneratedData/43/43th.py",['lst','ele'],[],['Reverse'],['10','11','12','13','14','15'],[],50)
# generateClones("../Data/GeneratedData/44/44th.py",['li1','li_copy','li2'],[],['Cloning'],['4', '8', '2', '10', '15', '18'],['Original', 'List', "After"],50)
# generateClones("../Data/GeneratedData/45/45th.py",["count",'lst', 'x','ele'],[],['countX'],[ '0' , '6', '20', '10', '8', '1'],["has", 'occurred', 'times'],50)
# generateClones("../Data/GeneratedData/46/46th.py",["total",'list1','ele' ],[],[],['11', '5', '17', '18', '23','0'],["Sum", "all" "elements" "given" "list"],50)
# generateClones("../Data/GeneratedData/47/47th.py",["myList","result", "list1","list2" ],[],['multiplyList'],["1", "2", "3", "4"],[],50)
# generateClones("../Data/GeneratedData/48/48th.py",["list1"],[],[],["10", '20', '4', "45", "99", "1"],["Smallest", "element", "is"],50)
# generateClones("../Data/GeneratedData/49/49th.py",["list1"],[],[],["10", "20", "4", "45", "99"],["Second" ,"largest" ,"element" ,"is"],50)
# generateClones("../Data/GeneratedData/50/50th.py",["list1", "N", "final_list", "i", "max1", "j"],[],['Nmaxelements'],["2", "41", "85", "0", '3', "7", "6", "10"],[],50)
# generateClones("../Data/GeneratedData/51/51st.py",["list1", "num"],[],[],["10", "21", "4", "45", '66', "93"],[],50)
# generateClones("../Data/GeneratedData/52/52nd.py",["start", "endd", "num"],[],[],["4", "19" ],[],50)
# generateClones("../Data/GeneratedData/53/53rd.py",["arr", "i", "key", "j"],[],['insertionSort'],["12", "11", "13", "5", "6"] ,["Sorted", "array", "is"],50)
# generateClones("../Data/GeneratedData/54/54th.py",["list1", "even_count", "odd_count", "num"],[],[],["10", "21", "4", "45", "66", "93"],["Even" ,"numbers" ,"in", "the", "list" ,"Odd"],50)
# generateClones("../Data/GeneratedData/55/55th.py",["list1","num"],[],[],["11", "-21","45", "66", "-93"],["stringsList"],50)
# generateClones("../Data/GeneratedData/56/56th.py",["list1", "neg_nos", "num"],[],[],["-10", "-21", "-4", "45", "-66", "93"],["Negative" ,"numbers" ,"the" ,"list"],50)
# generateClones("../Data/GeneratedData/57/57th.py",["start", "endss", "num"],[],[],["0"],[ "endd","Enter" ,"the" ,"startt" ,"of" ,"range"],50)
# generateClones("../Data/GeneratedData/58/58th.py",["list1", "pos_count","neg_count","num"],[],[],['10', "-21", "4", "-45", "66", "-93", '1', "0"],["Negative", "Positive" ,"numbers", "the", "list"],50)
# generateClones("../Data/GeneratedData/59/59th.py",["list1","ele"],[],[],["11", "5", "17", "18", "23", "50"],["New", "list", "after", "removing", "all", "even", "numbers"],50)
# generateClones("../Data/GeneratedData/60/60th.py",["tuples","t"],[],['Remove'],['15','8','45'],["ram", "laxman", "sita","krishna", "akbar"],500)
# generateClones("../Data/GeneratedData/61/61st.py",["_size", "x", "i", "k", "j", "repeated", "list1"],[],['Repeat'],["10", "30", "20", "40", "50", "60","20"],[],50)
# generateClones("../Data/GeneratedData/62/62nd.py",["lists","cu_list", "length","x"],[],['Cumulative'],['10', '20', '30', '40', '50'],[],50)
# generateClones("../Data/GeneratedData/63/63rd.py",["my_list", "i", "n", "l", "x"],[],['divide_chunks'],[],['geeks', 'forr', 'like', 'geeky','nerdy', 'geek', 'love', 'questions','words', 'life'],50)
# generateClones("../Data/GeneratedData/64/64th.py",["list1", "list2", "zipped_pairs", "y", "x", "z","_"],[],['sort_list'],[ "0",   "1",   "2", "1"],["stringsList"],50)
# generateClones("../Data/GeneratedData/65/65th.py",["list", "n"],[],[],["2", "4", "6", "8"],[],50)
# generateClones("../Data/GeneratedData/66/66th.py",["arr", "size", "i"],[],[],["2", "1"],[],50)
# generateClones("../Data/GeneratedData/67/67th.py",["test_list", "n", "key_list", "res","idx"],[],[],["3", "8","10","18","33"],["Gfg", "is", "Best", "Geeks", "The", "original", "list" ,"name", "number", "constructed", "dictionary"],50)
# generateClones("../Data/GeneratedData/68/68th.py",["test_dict", "temp", "key", "val", "res"],[],[],["7", "15", "5", "9", "19", "10", "12", "20", "2"],["The", "original", 'dictionary', "extracted", "values" , "is",'Gfg','best', "b", "c", "a" ],50)
# generateClones("../Data/GeneratedData/69/69th.py",["test_list", "res", "idx" ],[],[],['5', '6', '7','8', '3', '2','8', '2', '1'],["The" ,"constructed" ,"dictionary" ,"original" ,"list" ,"is" ],50)

# generateClones("../Data/GeneratedData/70/70th.py",['sss','ans'],[],['isPalindrome'],['-1'],['Yes','No',"malayalam"],50)
# generateClones("../Data/GeneratedData/71/71th.py",['sss','reverse_sentence','input','words'],[],['rev_sentence'],[],['geeks', 'quiz', 'practice code'],50)
# generateClones("../Data/GeneratedData/72/72th.py",['test_str','new_str'],[],[],['2'],["GeeksForGeeks","The string", 'after','removal',"of i'th","character :","The original",'string is',":"],50)
# generateClones("../Data/GeneratedData/73/73th.py",['sub_str','string'],[],['check'],['-1'],['geek','geeks','for','geeks','YES',"NO"],50)
# generateClones("../Data/GeneratedData/74/74th.py",['ss','word'],[],['printWords'],[],['i','am','muskan'],50)
# generateClones("../Data/GeneratedData/75/75th.py",['string','vowels','char','ssww'],[],['check'],[],['aeiou','Not','Accepted',"SEEquoiaL"],50)
# generateClones("../Data/GeneratedData/76/76th.py",['kkk','strrr','x','text'],[],['string_k'],['3'],["geek for geeks"],50)
# generateClones("../Data/GeneratedData/77/77th.py",['ttt','iii','attemptThis','attemptNext','completed','iteration'],[],[],['0','1'],["geek","Target matched after","iterations"],50)
# generateClones("../Data/GeneratedData/78/78th.py",['string','regex'],[],['run'],[],["Geeks$For$Geeks","String","is","not",'accepted'],50)
# generateClones("../Data/GeneratedData/79/79th.py",['str'],[],['removeDupWithoutOrder','removeDupWithOrder'],[],["geeksforgeeks","Without",'Order =',"With"],50)
# generateClones("../Data/GeneratedData/80/80th.py",['ccc','jjj','iii','str1','str2'],[],['count'],['0'],['No.','of matching','characters are :','aabcddekll12@','bb2211@55k','str1','str2'],50)
# generateClones("../Data/GeneratedData/81/81th.py",['stringg','iii','aaa','bbb'],[],['remove'],['5'],["geeksFORgeeks"],50)
# generateClones("../Data/GeneratedData/82/82th.py",['new_string','stringg','list_string'],[],['split_string','join_string'],[],['Geeks ' ,'for','Geeks'],50)
# generateClones("../Data/GeneratedData/83/83th.py",['string','ppp','sss'],[],['check'],[],['0','1'],50)
# generateClones("../Data/GeneratedData/84/84th.py",['patterns','word'],[],['closeMatches'],[],['appel','ape', 'apple', 'peach', 'puppy'],50)
# generateClones("../Data/GeneratedData/85/85th.py",['Aaa','Bbb','count','word'],[],['UncommonWords'],['0','1'],["Learning", 'from' ,'Geeks','for'],50)
# generateClones("../Data/GeneratedData/86/86th.py",['str1','maketranss','final','strrr'],[],['Replace'],[],[],50)
# generateClones("../Data/GeneratedData/87/87th.py",['strrr','strrraa','perm','permList'],[],['allPermutations'],[],['ABC'],50)
# generateClones("../Data/GeneratedData/88/88th.py",['jjj','iii','WC','input'],[],['find_dup_char'],['1'],['geeksforgeeks'],50)
# generateClones("../Data/GeneratedData/89/89th.py",['num','LOC','fact','iii'],[],['factorial','exec_code'],['1','5'],[],50)
# generateClones("../Data/GeneratedData/90/90th.py",['input','ddd','Lfirst','Lsecond','Rfirst','Rsecond','das'],[],['rotate'],['2'],['Left','Right','Rotation','GeeksforGeeks'],50)
#generateClones("../Data/GeneratedData/91/91th.py",['','','','','',''],50)
#generateClones("../Data/GeneratedData/92/92th.py",['','','','','',''],50)
#generateClones("../Data/GeneratedData/93/93th.py",['','','','','',''],50)
#generateClones("../Data/GeneratedData/94/94th.py",['','','','','',''],50)
#generateClones("../Data/GeneratedData/95/95th.py",['','','','','',''],50)
#generateClones("../Data/GeneratedData/96/96th.py",['','','','','',''],50)
#generateClones("../Data/GeneratedData/97/97th.py",['','','','','',''],50)
#generateClones("../Data/GeneratedData/98/98th.py",['','','','','',''],50)
#generateClones("../Data/GeneratedData/99/99th.py",['','','','','',''],50)
#generateClones("../Data/GeneratedData/100/100th.py",['','','','','',''],50)







