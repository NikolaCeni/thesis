# welcome to the comments generator
# find some instructions below


import random
import string
import fileinput
import os


class CommentsGenerator(object):

    def __init__(self):
        pass

    def add_n_comments_to_file(self, filename, number_of_comments):
        '''
        Adds n comments to a given file
        '''
        code_lines = self.__get_lines_from_file(filename)
        code_lines = self.__add_comments_to_lines(code_lines, number_of_comments)
        self.__write_file(filename, code_lines)

    def add_random_comments_to_files_in_dir(self, dir):
        '''
        Adds random number of comment to all the files in a directory
        '''
        files = self.__get_all_py_files_from_dir(dir)
        for filename in files:
            code_lines = self.__get_lines_from_file(filename)
            number_of_comments = max(random.randint(2, 6), int(0.1 * len(code_lines)))
            code_lines = self.__add_comments_to_lines(code_lines, number_of_comments)
            self.__write_file(filename, code_lines)

    def __get_lines_from_file(self, filename):
        '''
        Reads the liness from a given file, splitted by new line char
        '''
        f = open(filename, "r")
        code = f.read()
        lines = code.splitlines()
        return lines

    def __add_comments_to_lines(self, code_lines, number_of_comments):
        '''
        Adding comments to list of lines
        '''
        if number_of_comments > len(code_lines):
            number_of_comments = len(code_lines)

        lines_number = []
        for i in range(number_of_comments):
            lines_number.append(random.choice([x for x in range(len(code_lines)) if x not in lines_number]))

        for j, i in enumerate(lines_number):
            n = random.randint(0,10)
            if n in[1,3]:
                code_lines[i] += "\n#" + "comment" + str(j)
            elif n in [6,7]:
                code_lines[i] += " #" + "comment" + str(j)+"\n#" + "comment" + str(j+1)
            else:
                code_lines[i] += " #" + "comment" + str(j)

        return code_lines

    def __write_file(self, filename, code):
        '''
        Write into a file
        '''
        f = open(filename, 'w')
        for line in code:
            f.write(line + '\n')
        f.close()

    def __get_all_py_files_from_dir(self, dir):
        '''
        Getting all the python files from a given dir
        '''
        files = []
        for r, d, fs in os.walk(dir):
            for f in fs:
                if f.endswith(".py"):
                    files.append(os.path.join(r, f))
        return files

    def remove_empty_lines(self, filename):
        '''
        Removing empty lines from a file
        '''
        lines = []
        for line in fileinput.FileInput(filename, inplace=1):
            if line.rstrip():
                lines.append(line)

    def removeComments(self, filename):
        '''
        Remove comments from a file
        '''
        newFileName = filename
        f = open(filename, "r")
        code = ""
        for i in f.readlines():
            code += i.split('#')[0]
            if (i.split('#')[0] != '\n'):
                code += '\n'

        lines = code.split("\n")
        non_empty_lines = [line for line in lines if line.strip() != ""]
        string_without_empty_lines = ""
        for line in non_empty_lines:
            string_without_empty_lines += line + "\n"
        f = open(newFileName, "w")
        f.write(string_without_empty_lines)

    def remove_comments_all_files_dir(self, dir):
        '''
        Remvoing comments from all the files from a directoryl
        '''
        files = self.__get_all_py_files_from_dir(dir)
        for filename in files:
            self.removeComments(filename)



