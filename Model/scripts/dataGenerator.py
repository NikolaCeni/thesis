import os
import random
import csv
import numpy as np


class DataGenerator:

    def __init__(self):
        self.pairs_count = 40
        self.non_clones_count = 0
        self.clones_count = 0
        self.dataPath = 'Data/GeneratedData'
        self.trainingData = []

    def make_training_data(self):
        '''
        Creates training data samples
        '''
        self.__get_data_files()
        self.__read_data_files_and_write_to_csv()

    def __read_data_files_and_write_to_csv(self):
        '''
        Read the code pairs from the data samples and write them in csv file together with their label
        '''
        for x in self.trainingData:
            first = x[0][0]
            second = x[0][1]
            f = open(first, 'r')
            first_code = f.read()
            f.close()
            f = open(second, 'r')
            second_code = f.read()
            f.close()
            with open('data.csv', 'a') as file:
                writer = csv.writer(file)
                writer.writerow([first_code, second_code, x[1]])

    def __get_data_files(self):
        '''
        Creates the data sample triples (file1,file2,label)
        '''
        for i in os.listdir(self.dataPath):
            dir_path = self.dataPath + '/' + i
            self.trainingData += self.__generate_clones(dir_path, self.pairs_count)
            self.clones_count += self.pairs_count
        print(len(self.trainingData))
        self.trainingData += self.__generate_non_clones(self.dataPath, self.clones_count)
        print(len(self.trainingData))
        self.non_clones_count = self.clones_count
        np.random.shuffle(self.trainingData)

    @staticmethod
    def __generate_clones(dir_path, count):
        '''
        Generates count number of triples (file1,file2,label) which are clones from a given directory 
        '''
        res = []
        for i in range(0, count):
            a = random.randint(0, len(os.listdir(dir_path)) - 1)
            b = random.randint(0, len(os.listdir(dir_path)) - 1)
            res += [([dir_path + '/' + os.listdir(dir_path)[a], dir_path + '/' + os.listdir(dir_path)[b]], 1)]
        return res

    @staticmethod
    def __generate_non_clones(dir_path, count):
        '''
        Generates count number of triples (file1,file2,label) which are not clones from a given directory
        '''
        res = []
        i = 1
        while i <= count:
            a = random.randint(0, len(os.listdir(dir_path)) - 1)
            b = random.randint(0, len(os.listdir(dir_path)) - 1)

            if a != b:
                dir_path_a = os.listdir(dir_path + '/' + os.listdir(dir_path)[a])
                dir_path_b = os.listdir(dir_path + '/' + os.listdir(dir_path)[b])
                a1 = random.randint(0, len(dir_path_a) - 1)
                b1 = random.randint(0, len(dir_path_b) - 1)
                res += [([dir_path + '/' + os.listdir(dir_path)[a] + '/' + dir_path_a[a1],
                          dir_path + '/' + os.listdir(dir_path)[b] + '/' + dir_path_b[b1]], 0)]
                i += 1
        return res


DataGenerator().make_training_data()
