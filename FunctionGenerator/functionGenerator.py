from keyword import iskeyword
import tokenize
import string


class FunctionGenerator:
    '''
    Class used for function generation out of a cloned pair
    '''

    def __init__(self,editor_path):
        self.editor_path = editor_path
        self.abc = string.ascii_lowercase
        self.abc_count = len(self.abc)
        self.variable_count = 0
        self.function_count = 0
        self.empty_chars = ['\n', '\t', '', ' ']
        self.symbols = ['+', '-', '/', '//', '&', '^', '~', '|', '**', '<<', '>>', '<', '>', '%', '*', '@', '==', '!=',
                        '>=', '<=', '=', '\\', ',', '#', '\'\'\'', ',', '.', '\'', '"', ':', '[', ']', '{', '}', '(',
                        ')']
        self.operators = ['+', '-', '==', '=', '*', '%', '<', '>', '<=', '>=']
        self.functions = ['print', 'int', 'float', 'str', 'char', 'input', 'utf-8']

    def reset_counters(self):
        '''
        Resets the variables and fucntions counter
        '''
        self.variable_count, self.function_count = 0, 0

    def generate_function_given_codes(self, first_seg, sec_seg):
        '''
        Combines removing comments, tokenizing and creating function
        Returns the generated function
        '''
        first_seg = self.remove_comments_from_segment(first_seg)
        sec_seg = self.remove_comments_from_segment(sec_seg)
        segment = self.get_shorter_segment(first_seg, sec_seg)
        self.function_out_of_code(segment)
        return self.function

    def get_shorter_segment(self, first_seg, sec_seg):
        '''
        Given two segments, returns the one having less lines
        '''
        first_seg_lines, sec_seg_lines = first_seg.split('\n'), sec_seg.split('\n')
        if (len(first_seg_lines) > len(sec_seg_lines)):
            return first_seg
        return sec_seg

    def remove_comments_from_segment(self, segment):
        '''
        Removes comments from a file, and reewrites its content
        '''
        segment_without_comments = self.remove_comments_replace_padded_space(segment)
        return segment_without_comments

    def remove_comments_replace_padded_space(self, code):
        '''
        Removes the comments from a code
        '''
        code_lines = code.split('\n')
        for i in range(len(code_lines)):
            code_lines[i] = (code_lines[i].split('#')[0])
        code = ''
        for i in code_lines:
            if (not self.is_empty(i)):
                code += self.replace_leading_spaces_with_stars(i) + '\n'
        code = code[:-1]
        return code

    def replace_leading_spaces_with_stars(self, line):
        '''
        Replaces the leading spaces of a line with stars
        '''
        i = 0
        res = ''
        while (i < len(line) and line[i] in [' ', '\t']):
            if (line[i] == ' '):
                res += '*'
            else:
                res += ''.join(['*' for i in range(8)])
            i += 1
        res += line[i:]
        return res

    def function_out_of_code(self, segment):
        '''
        Creates a function out of given code and stores it as self.function
        '''
        self.function = ''
        self.variable_count = 0
        variables_pairs = []
        words = []
        filename = self.editor_path+'FunctionGenerator\\function_generating.txt'
        f = open(filename, 'w')
        f.write(segment)
        f.close()
        with open(filename, 'rb') as f:
            tokens = tokenize.tokenize(f.readline)
            for token in tokens:
                words.append(self.handle_word(token.string, variables_pairs))
        function_def = ''.join(words)
        function_sig = 'def fun' + str(self.function_count) + '(' + ''.join([i[1] + ',' for i in variables_pairs])[
                                                                    :-1] + '):'
        self.function = function_sig + '\n' + ''.join(['    ' + i + '\n' for i in function_def.split('\n')])
        self.function_count += 1

    def handle_word(self, word, variables_pairs):
        '''
        handles a given token(word) according to its type
        '''
        cnt = self.count_leading_stars(word)
        word = word[cnt:]
        if (iskeyword(word)):
            word = word + ' '
        elif (word == 'utf-8'):
            return ''
        elif (word == '\r\n'):
            word = '\n'
        elif (self.is_empty(word)):
            pass
        elif (word in self.symbols):
            pass
        elif (word in self.operators):
            pass
        elif (word in self.functions):
            pass
        else:
            if (word in [i[0] for i in variables_pairs]):
                index = [i[0] for i in variables_pairs].index(word)
                word = variables_pairs[index][1]
            else:
                var = self.create_variable()
                variables_pairs.append((word, var))
                word = var
        return ''.join([' ' for i in range(cnt)]) + word

    def count_leading_stars(self, word):
        '''
        Counts leading stars
        '''
        i = 0
        while (i < len(word) and word[i] == '*'):
            i += 1
        return i

    def is_empty(self, word):
        '''
        Checks if a given word is empty (spaces or tabs only)
        '''
        return len([i for i in word if i not in ['', ' ', '\t']]) == 0

    def create_variable(self):
        '''
        Returns variable name which is unique
        I stars by returning single characters from the alphabet,
        but if that is not enough it starts adding 'a' as a prefix
        as many times as needed
        '''
        var = ''.join(['a' for i in range(self.variable_count // self.abc_count)])
        var += self.abc[self.variable_count % self.abc_count]
        self.variable_count += 1
        return var
