import pytest
import os
from FunctionGenerator.functionGenerator import FunctionGenerator

'''
File which tests the behavior of the function generation feature by testing 4 different scenarios
'''


def get_editor_path():
    '''
    Returns the path of the project on the local machine
    '''
    current_path = os.getcwd()
    editor_path = ''
    for word in current_path.split('\\')[:-1]:
        editor_path += word + '\\'
    print(editor_path)
    print(current_path)
    return editor_path


@pytest.fixture
def generator():
    '''
    Recreates the function generator
    '''
    functionGenerator = FunctionGenerator(editor_path=get_editor_path())
    return functionGenerator


def test_one_fg(generator):
    '''
    Case when there are multiple-line segments
    '''
    code1 = '''
    sth = []
    for s in gf:
        if s.endswith(".py"):
	    #what is this code about
        sth.append(s)
    '''
    code2 = '''
    newlist = [] 
    #good morning
    for names in items:
        if names.endswith(".txt"):
            newlist.append(names)
    '''
    expected_function = '''def fun0(a,b,c,d,e,f):
        a=[]
        for bin c:
            if b.d(e):
                a.f(b)'''
    result = generator.generate_function_given_codes(code1, code2)
    assert result.strip() == expected_function.strip()


def test_two_fg(generator):
    '''
    Single-line print function
    '''
    code1 = '''
    print (newlist)
    '''
    code2 = '''
    print (sthh)
    '''
    expected_function = '''def fun0(a):
        print(a)
        '''
    result = generator.generate_function_given_codes(code1, code2)
    assert result.strip() == expected_function.strip()


def test_three_fg(generator):
    '''
    Single line function call
    '''
    code1 = '''
    pg= os.listdir(".")
    '''
    code2 = '''
    gf= os.listdir(".")
    '''
    expected_function = '''def fun0(a,b,c,d):
        a=b.c(d)
        '''
    result = generator.generate_function_given_codes(code1, code2)
    assert result.strip() == expected_function.strip()


def test_four_fg(generator):
    '''
     Single-line print function call-checking the consistency of the result
    '''
    code1 = '''
    items = os.listdir(".")
    '''
    code2 = '''
    pg= os.listdir(".")
    '''

    expected_function = '''def fun0(a,b,c,d):
        a=b.c(d)
        '''
    result = generator.generate_function_given_codes(code1, code2)
    assert result.strip() == expected_function.strip()
