import pytest
import os
from Model.model import Model

'''
Testing the model prediction.
'''
def get_editor_path():
    '''
    Returns the path of the project on the local machine
    '''
    current_path = os.getcwd()
    editor_path = ''
    for word in current_path.split('\\')[:-1]:
        editor_path += word+'\\'
    print(editor_path)
    print(current_path)
    return editor_path

model = Model(editor_path=get_editor_path())

code1a_clones = '''
sum = 0
for i in range(10):
    print(i)
    sum+=i
    '''
code2a_clones = '''
sum = 0
for i in range(10):
    print(i)
    #comment
    sum+=i
    '''

code1b_clones = '''
if (condition):
    print('True')
else:
    print('False')
    '''
code2b_clones = '''
if (condition):
#if it is true
    print('True')
else:
    print('False')
    '''

code1c_clones = '''
s1 = (-b-cmath.sqrt(d))/(2*a)
s2 = (-b+cmath.sqrt(d))/(2*a)
if (s1!=s2):
    print(s1)
    print(s2)
else: 
    print(s1)
'''
code2c_clones = '''
x1 = (-bb-cmath.sqrt(da))/(2*aa)
x2 = (-bb+cmath.sqrt(da))/(2*aa)
if (x1!=x2):
    print(x1)
    print(x2)
else:
    print(x1)
'''


@pytest.mark.parametrize("code1,code2", [(code1a_clones, code1a_clones), (code1a_clones, code2a_clones),
                                         (code1b_clones, code2b_clones), (code1c_clones, code2c_clones)])
def test_model_clones(code1, code2):
    '''
    Testing the model when clones are given (default threshold value used)
    '''
    (prediction, similarity) = model.predict(code1, code2)
    print(similarity)
    assert prediction


code1_nonclones = '''
sum = 0
#summing numbers
for i in range(10):
    print(i)
    sum+=i
    '''

code2_nonclones = '''
if (condition):
#if it is true
    print('True')
else:
    print('False')
    '''

code3_nonclones = '''
l = True
i = 0
while i<10 and l:
    print(i)
    l = i % 5 ==2 
'''

code4_nonclones = '''
a = 1
b = 5
c = 6'''


@pytest.mark.parametrize("code1,code2", [(code1_nonclones, code2_nonclones), (code3_nonclones, code2_nonclones),
                                         (code1_nonclones, code3_nonclones), (code1c_clones, code1a_clones),
                                         (code2c_clones, code1b_clones), (code4_nonclones, code2_nonclones),
                                         (code4_nonclones, code1b_clones), (code2a_clones, code3_nonclones),
                                         (code4_nonclones, code1b_clones), (code2c_clones, code2_nonclones)])
def test_model_nonclones(code1, code2):
    '''
    Testing the model when non-clones are given (default threshold value used)
    '''
    (prediction, similarity) = model.predict(code1, code2)
    assert prediction == False
