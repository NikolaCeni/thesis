import pytest

from Editor.AuxFiles.codeSegmentator import CodeSegmentator

'''
Testing the segmentation startegies based on the number of segments obtained
'''
# -------------------------------------------------------------------
code1 = '''import os

items = os.listdir(".")

newlist = [] 
#good morning
for names in items:
    if names.endswith(".txt"):
        newlist.append(names)
print (newlist)


gf= os.listdir(".")

sth = []
for s in gf:
    if s.endswith(".py"):
	#what is this code about
        sth.append(s)
print (sthh)



pg= os.listdir(".")

df= []
for qwqrw in sd:
    if ssss.endswith("nikola"):
        newlist.append(dddd) #code clones
print (rrrr)
'''
code1_lines = code1.split('\n')
# -------------------------------------------------------------------
code2 = '''from test1_aux import *

print("Nikola")

import random
print(random.randint(1,24))

print("a")'''
code2_lines = code2.split('\n')
# -------------------------------------------------------------------
code3 = '''# Solve the quadratic equation ax**2 + bx + c = 0

# import complex math module
import cmath

a = 1
b = 5
c = 6

# calculate the discriminant
d = (b**2) - (4*a*c)

# find two solutions
sol1 = (-b-cmath.sqrt(d))/(2*a)
sol2 = (-b+cmath.sqrt(d))/(2*a)

print('The solution are {0} and {1}'.format(sol1,sol2))
'''
code3_lines = code3.split('\n')
segmentator = CodeSegmentator()


# -------------------------------------------------------------------


@pytest.mark.parametrize("code_lines,seg_count", [(code1_lines, 20), (code2_lines, 7), (code3_lines, 12)])
def test_strategy0(code_lines, seg_count):
    '''
    Tests strategy 0
    '''
    segmentator.set_strategy(0)
    segments = segmentator.separate_segments(code_lines)
    assert len(segments) == seg_count


@pytest.mark.parametrize("code_lines,seg_count", [(code1_lines, 20), (code2_lines, 7), (code3_lines, 12)])
def test_strategy1(code_lines, seg_count):
    '''
    Tests strategy 1
    '''
    segmentator.set_strategy(1)
    segments = segmentator.separate_segments(code_lines)
    assert len(segments) == seg_count


@pytest.mark.parametrize("code_lines,seg_count", [(code1_lines, 8), (code2_lines, 1), (code3_lines, 2)])
def test_strategy2(code_lines, seg_count):
    '''
    Tests strategy 2
    '''
    segmentator.set_strategy(2)
    segments = segmentator.separate_segments(code_lines)
    assert len(segments) == seg_count


@pytest.mark.parametrize("code_lines,seg_count", [(code1_lines, 5), (code2_lines, 1), (code3_lines, 2)])
def test_strategy3(code_lines, seg_count):
    '''
    Tests strategy 3
    '''
    segmentator.set_strategy(3)
    segments = segmentator.separate_segments(code_lines)
    assert len(segments) == seg_count


@pytest.mark.parametrize("code_lines,seg_count", [(code1_lines, 17), (code2_lines, 7), (code3_lines, 12)])
def test_strategy4(code_lines, seg_count):
    '''
    Tests strategy 4
    '''
    segmentator.set_strategy(4)
    segments = segmentator.separate_segments(code_lines)
    assert len(segments) == seg_count
