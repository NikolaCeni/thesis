import os
import subprocess
import Editor.AuxFiles.auxFunctions as aux
from tkinter import filedialog
from tkinter import simpledialog
from Editor.AuxFiles.auxFunctions import *


class BasicButtonsFunc:
    '''
    Class that implements all the features of thee editor not related to code clones
    '''

    def __init__(self):
        pass

    def handle_key_pressed(self, editor):
        '''
        Event handler for key pressed in the root
        '''
        if editor.has_tabs():
            editor.syntaxHighlighter.highlight_syntax(editor)

    def handle_closing_file(self, editor):
        if editor.has_tabs():
            root = tk.Toplevel()
            root.grab_set()
            root.title('Save file')
            message_label = tk.Label(root, font=editor.code_font,
                                     text='Do you want to save the\nfile before closing?')
            message_label.pack(side='top', padx=20, pady=20)
            options_frame = tk.Frame(root)
            options_frame.pack(side='top')
            save_button = tk.Button(options_frame, text='Yes',
                                    command=lambda: self.close_and_save_file(editor, root=root))
            save_button.pack(side='left', padx=10, pady=10)
            dont_save_button = tk.Button(options_frame, text='No',
                                         command=lambda: self.close_file(editor, root=root))
            dont_save_button.pack(side='left', padx=10, pady=10)
            root.mainloop()

    def close_and_save_file(self, editor, root=None):
        '''
        Saves and closes a single tab
        '''
        self.save(editor)
        self.close_file(editor)
        if (root != None):
            root.destroy()

    def close_and_save_all(self, editor):
        '''
        Closes and saves all the tabs in the editor
        '''
        while editor.has_tabs():
            self.save(editor)
            self.close_file(editor)

    def close_all(self, editor):
        '''
        Closes all the tabs in the editor
        '''
        while editor.has_tabs():
            self.close_file(editor)

    def close_file(self, editor, root=None, index=None):
        '''
        Close the current tab
        '''
        if (editor.has_tabs()):
            if index == None:
                current_tab_index = editor.tab_control.index('current')
            else:
                current_tab_index = index
            editor.paths.pop(current_tab_index)
            editor.code_parts.pop(current_tab_index)
            editor.tab_frames.pop(current_tab_index)
            editor.tab_control.forget(current_tab_index)
            if (not editor.has_tabs()):
                editor.hide_output_frame()
                editor.tab_control.pack_forget()
                editor.welcome_label.pack(side='top', expand=True, fill='both')
                editor.home_label.pack(side='top', expand=True, fill='both')
        if (root != None):
            root.destroy()

    def refresh_tabs(self, editor):
        '''
        Refreshes the content in the tabs
        Useful in case a file is edited using some other editor
        '''
        index = 0
        error_paths = []
        while index < len(editor.paths):
            path = editor.paths[index]
            if os.path.exists(path):
                self.refresh_tab(editor, index, path)
                index += 1
            else:
                error_paths.append(path)
                self.close_file(editor, root=None, index=index)
        if (len(error_paths) != 0):
            error_popup_text = 'The following paths don\'t exist:\n'
            for error_path in error_paths:
                error_popup_text += error_path + '\n'
            error_popup_text += '\nThey will be removed from the editor!'
            self.error_popup(editor, msg=error_popup_text, title='File name error')

    def refresh_tab(self, editor, index=None, path=None):
        '''
        Refreshes a single tab
        '''
        if editor.has_tabs():
            try:
                if index == None:
                    index = editor.tab_control.index('current')
                if path == None:
                    path = editor.paths[index]
                code = open(path, 'r').read()
                code_part = editor.code_parts[index]
                code_part.delete('1.0', 'end')
                code_part.insert('1.0', code)
                editor.syntaxHighlighter.highlight_syntax(editor, code_part=code_part)
            except FileNotFoundError:
                self.error_popup(editor, msg='The filename is changed.\n' + path + ' doesn\'t exist.',
                                 title='File name error')
                self.close_file(editor, root=None, index=index)

    def add_file(self, editor):
        '''
        Adds a new file and creates a separate tab for it
        '''
        (folder_path, file_name) = self.ask_folder_and_file_name(editor)
        if (folder_path != None and file_name != None):
            file_path = folder_path + '/' + file_name
            open(os.path.join(folder_path, file_name), "w")

            code_part = editor.create_tab(file_path)
            code_part.save_content()

    def ask_folder_and_file_name(self, editor):
        '''
        Creates pop-ups for folder and file name input
        '''
        folder_path = filedialog.askdirectory()
        if folder_path == '':
            self.error_popup(editor, 'No folder selected!', 'Folder selection error')
            return (None, None)
        file_name = simpledialog.askstring(title="Insert file name",
                                           prompt="Insert name for the file\nHas to be .py or .txt")
        if file_name != None and is_empty_or_new_line(file_name.split('.')[0]):
            self.error_popup(editor, 'The name of the file must not be empty!', 'File name error')
            return (None, None)
        if (file_name != None and file_name.split('.')[-1] not in editor.file_extensions):
            self.error_popup(editor, 'The extension of the file is not accepted!', 'File extension error')
            return (None, None)

        return (folder_path, file_name)

    def open_file(self, editor, file_path=None):
        '''
        Imports a code from a selected file and creates a separate tab for it
        '''
        if (file_path == None):
            file_path = filedialog.askopenfilename()
        if (file_path.split('.')[-1] in editor.file_extensions):
            try:
                code = open(file_path, 'r').read()
                code_part = editor.create_tab(file_path)
                code_part.delete('1.0', 'end')
                code_part.insert('1.0', code)
                code_part.save_content()
                editor.syntaxHighlighter.highlight_syntax(editor, code_part=code_part)
            except Exception:
                pass
        elif file_path == '':
            self.error_popup(editor, "No file selected!", 'No file selected error')
        else:
            self.error_popup(editor, "The file extension has to be .py, .csv or .txt", "File extension error")

    def error_popup(self, editor, msg, title):
        '''
        Creates a pop up with given error message shown when a file of wrong format is selected
        '''
        editor.unbind_shortcuts()
        aux.unbind_all_code_parts_in_editor(editor)

        root = tk.Toplevel()
        root.grab_set()
        root.wm_title(title)
        root.protocol("WM_DELETE_WINDOW", lambda: self.error_popup_destroy(editor, root))
        label = tk.Label(root, text=msg, font=editor.code_font)
        label.pack(side="top", fill="x", pady=10, padx=50)
        B1 = tk.Button(root, text="Okay", command=lambda: self.error_popup_destroy(editor, root))
        B1.pack(pady=10)

    def error_popup_destroy(self, editor, root):
        '''
        Handes the destorying of the error popup window and binds back the shortcuts
        '''
        root.destroy()
        editor.bind_shortcuts()
        aux.bind_all_code_parts_in_editor(editor)

    def clear_output(self, editor):
        '''
        Clears the output text from the output block
        '''
        editor.set_output('')

    def clear_code_window(self, editor):
        '''
        Clears the code written in the current tab
        '''
        if (editor.has_tabs()):
            code_part = editor.get_current_code_part()
            code_part.save_content()
            code_part.delete('1.0', 'end-1c')
            self.clear_output(editor)
            editor.hide_output_frame()

    def handle_undo(self, editor, shortcut=True):
        '''
        Event handler for Ctrl+Z
        '''
        if editor.has_tabs():
            code_part = editor.get_current_code_part()
            code_part.undo_content(shortcut=shortcut)
            editor.syntaxHighlighter.highlight_syntax(editor)

    def save(self, editor):
        '''
        Saves the content of the current tab in its file
        '''
        if (editor.has_tabs()):
            current_tab_index = editor.tab_control.index('current')
            file_path = editor.paths[current_tab_index]
            code_part = editor.code_parts[current_tab_index]
            code = code_part.get('1.0', 'end-1c')
            f = open(file_path, 'w')
            f.write(code)

    def run_code(self, editor):
        '''
        Runs the code from the current tab
        '''
        if (editor.has_tabs()):
            current_tab_index = editor.tab_control.index('current')
            path_raw = editor.paths[current_tab_index]
            path = '"' + path_raw + '"'
            if path_raw.split('.')[-1] == 'py':
                try:
                    out = subprocess.check_output('python3 ' + path, stderr=subprocess.STDOUT).decode()
                    error = False
                except Exception as e:
                    if b'Python was not found' in e.output:
                        try:
                            out = subprocess.check_output('python ' + path, stderr=subprocess.STDOUT).decode()
                            error = False
                        except Exception as e2:
                            if b'Python was not found' in e2.output:
                                out = 'Please Install Python'
                            else:
                                out = e2.output
                            error = True
                    else:
                        out = e.output
                        error = True
                editor.set_output(out, error=error)
            else:
                self.error_popup(editor, 'Only python files can be executed!', 'Wrong file type executed')

    def save_and_run(self, editor):
        '''
        Saves and runs a single file/code
        '''
        self.save(editor)
        self.run_code(editor)

    def change_theme(self, editor):
        '''
        Changes the editor's theme by changing the colors of the specific widgets
        '''
        if (editor.color == 'lightblue'):
            editor.color, editor.header_footer_color = 'silver', 'whitesmoke'
            editor.output_bg, editor.output_text = 'black', 'white'
            editor.code_color, editor.cursor_color, editor.code_bg_color = 'black', 'black', 'white'
            editor.panned_window_bg_color = 'grey64'
            editor.keywords_color, editor.loop_keywords_color, editor.logical_values_color, editor.logical_operators_color, editor.class_keywords_color = 'blue', 'green', 'darkorange', 'green', 'purple'
        else:
            editor.color, editor.header_footer_color = 'lightblue', 'lightslategrey'
            editor.output_bg, editor.output_text = 'white', 'black'
            editor.code_color, editor.cursor_color, editor.code_bg_color = 'white', 'white', 'dimgray'
            editor.panned_window_bg_color = 'grey80'
            editor.keywords_color, editor.loop_keywords_color, editor.logical_values_color, editor.logical_operators_color, editor.class_keywords_color = 'aquamarine', 'limegreen', 'gold', 'limegreen', 'skyblue'
        for ind, code_part in enumerate(editor.code_parts):
            code_part.config(insertbackground=editor.cursor_color, bg=editor.code_bg_color,
                             fg=editor.code_color, font=editor.code_font)
            editor.configure_tags(code_part)
            editor.tab_frames[ind].config(bg=editor.color)
        editor.root.config(bg=editor.color)
        editor.home_label.config(bg=editor.color)
        editor.welcome_label.config(bg=editor.color)
        editor.header.config(bg=editor.header_footer_color)
        editor.footer.config(bg=editor.header_footer_color)
        editor.output_buttons_frame.config(bg=editor.header_footer_color)
        editor.output_label.config(bg=editor.header_footer_color)
        editor.seg_strategy_label.config(bg=editor.header_footer_color)
        editor.button_meaning_label.config(bg=editor.header_footer_color)
        editor.output_part.config(foreground=editor.output_text, bg=editor.output_bg)
        editor.code_paned_window.config(bg=editor.header_footer_color)
        editor.general_paned_window.config(bg=editor.panned_window_bg_color)
