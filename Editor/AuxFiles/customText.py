import tkinter as tk


class CustomText(tk.Text):
    '''
    A class used for creating a text widget with a new method, highlight_pattern()
    '''

    def __init__(self, *args, **kwargs):
        tk.Text.__init__(self, *args, **kwargs)
        self.content = []
        self.undo_count = 0
        self.bind('<Key>', lambda x: self.save_content())

    def bind_file_open_shortcut(self, function):
        '''
        Binds the ctrl+o shortcut to a specific function(this is done cause by default ctrl+o adds new line)
        '''
        self.bind('<Control-o>', function)
        self.bind('<Control-O>', lambda e: function)

    def highlight_pattern(self, pattern, tag, start="1.0", end="end",
                          regexp=False):
        '''
        Apply the given tag to all text that matches the given pattern
        If 'regexp' is set to True, pattern will be treated as a regular
        expression according to Tcl's regular expression syntax.
        '''
        if (pattern in ["#"]):
            self.handleComments(pattern, tag)
        else:
            start = self.index(start)
            end = self.index(end)
            self.mark_set("matchStart", start)
            self.mark_set("matchEnd", start)
            self.mark_set("searchLimit", end)

            count = tk.IntVar()
            while True:
                index = self.search(pattern, "matchEnd", "searchLimit",
                                    count=count, regexp=regexp)
                if index == "": break
                if count.get() == 0: break
                self.mark_set("matchStart", index)
                self.mark_set("matchEnd", "%s+%sc" % (index, count.get()))
                self.tag_add(tag, "matchStart", "matchEnd")

    def handleComments(self, pattern, tag):
        '''
        Handles highlighting a comment
        '''
        all_lines = self.get('1.0', 'end').split('\n')
        if (pattern == '#'):
            for (ind, line) in enumerate(all_lines):
                try:
                    index = line.index('#')
                    self.tag_add(tag, str(ind + 1) + '.' + str(index), str(ind + 1) + '.end')
                except Exception as e:
                    pass

    def undo_content(self, shortcut=True):
        '''
        Undo the previous content of the widget
        '''

        if len(self.content) > 2:
            if (shortcut):
                self.content.pop(-1)
                self.content.pop(-1)
            content = self.content[-1]
            self.delete('1.0', 'end-1c')
            self.insert('1.0', content)
            self.content.pop(-1)

    def save_content(self):
        '''
        Saves the new content of the widget
        '''
        content = self.get('1.0', 'end-1c')
        self.content.append(content)
        if (len(self.content) > 40):
            self.content.pop(0)
