import tkinter as tk
import Editor.AuxFiles.auxFunctions as aux


class SingleInputPopup:
    '''
    Creating a popup window with only one Entry.
    It has title, main label, one entry and list of extra_labels
    Condition is 0 (the entry has to be in list)
    Condition is 1(the entry is between min and max)
    '''

    def __init__(self, editor, title, text, extra_labels_text, condition):

        self.value = None
        editor.unbind_shortcuts()
        aux.unbind_all_code_parts_in_editor(editor)

        self.root = tk.Toplevel()
        self.root.grab_set()
        self.root.geometry("370x220")
        self.root.protocol("WM_DELETE_WINDOW", lambda: self.close_popup(editor))
        self.root.resizable(False, False)
        self.root.title(title)

        main_label = tk.Label(self.root, text=text, font=('Monoid', 13))
        main_label.pack(side='top', pady=10, padx=10)

        self.entry = tk.Entry(self.root, width=10)
        self.entry.pack(side='top', padx=10, pady=3)

        for label_text in extra_labels_text:
            label = tk.Label(self.root, text=label_text, font=('Monoid', 13))
            label.pack(side='top', pady=4, padx=10)
        if condition == 0:
            button = tk.Button(self.root, text='See Strategies Description', font=('Monoid', 13),
                               command=lambda: aux.show_strategies_desc())
            button.pack(side='top', pady=10)
        button = tk.Button(self.root, text='Okay', font=('Monoid', 13),
                           command=lambda: self.check_and_exit(editor, condition))
        button.pack(side='top', pady=5)
        self.error_label = tk.Label(self.root, text='', foreground='red', font=('Monoid', 9))
        self.error_label.pack(side='top', pady=2)
        self.root.mainloop()

    def check_and_exit(self, editor, condition):
        '''
        Checks if the input value satisfies the given condition. If not it shows *Wrong input value
        '''
        input = self.entry.get()
        try:
            if (condition == 0):
                input = int(input)
                input_correct = input in editor.clonesButtonFunc.segmentator.strategies
            elif (condition == 1):
                input = float(input)
                input_correct = input >= editor.clonesButtonFunc.min_threshold and input <= editor.clonesButtonFunc.max_threshold
        except:
            input_correct = False
        if (input_correct):
            if (condition == 0):
                editor.clonesButtonFunc.segmentator.set_strategy(input)
                editor.change_clones_settings_item(index=1, text='Set Strategy (Current: ' + str(
                    editor.clonesButtonFunc.segmentator.strategy) + ')')
                editor.update_strategy_label()
            elif (condition == 1):
                editor.clonesButtonFunc.threshold = input
                editor.change_clones_settings_item(index=2, text='Set Similarity Threshold  (Current: ' + str(
                    editor.clonesButtonFunc.threshold) + ')')

            self.close_popup(editor)

        else:
            self.error_label.config(text='*Wrong input value')

    def close_popup(self, editor):
        '''
        Closing the popup and binding the shortcuts back
        '''
        self.root.destroy()
        editor.bind_shortcuts()
        aux.bind_all_code_parts_in_editor(editor)
