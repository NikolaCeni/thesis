import tkinter as tk
import subprocess

'''
File containing definitions of auxiliary functions
'''
def is_empty(text):
    '''
    Checks if a given text is empty
    '''
    return text == '' or len(text) == text.count(' ')

def is_empty_or_new_line(text):
    '''
    Checks if a given text is full on empty or has only \n or \t
    '''
    for i in text:
        if (i != '' and i != ' ' and i != '\t' and i != '\n'):
            return False
    return True
def write_to_file(file, text):
    '''
    Writes text into file
    '''
    f = open(file, 'w')
    f.write(text)


def not_in(list_of_pairs, list_elements):
    '''
    returns true if all elements from list_elements are NOT in list_of_pairs
    '''
    list_of_pairs_flattened = list(sum(list_of_pairs, ()))
    return len([i for i in list_elements if i in list_of_pairs_flattened]) == 0


def list_shortcuts():
    '''
    Opens the file with all the shortcuts
    '''
    subprocess.Popen(['PDFS\shortcuts.pdf'], shell=True)


def list_information():
    '''
    Opens information file about the IDE
    '''
    subprocess.Popen(['PDFS\\User_Manual.pdf'], shell=True)


def show_strategies_desc():
    '''
    Opens information file about the strategies of segmentation
    '''
    subprocess.Popen(['PDFS\Segmentation Strategies.pdf'], shell=True)


def is_segement_of_imports(code):
    '''
    Checks if a given code segment has imports in it
    '''
    if ('import ' in code):
        return True
    return False


def tab(text):
    '''
    Trying to take a tab being 4 spaces
    NOT USED
    '''
    print("tab pressed")
    text.insert(tk.INSERT, " " * 4)
    return 'break'


def is_one_line(segment):
    '''
    Checks if a segment is a single line or not
    '''
    return len(segment.split('\n')) == 1 or (len(segment.split('\n')) == 2 and segment.split('\n')[1] == '')

def custom_text_ctrl_o_handler(editor):
    '''
    Function handler binded to each code part in the editor
    '''
    editor.basicButtonFunc.open_file(editor)
    return "break"

def bind_all_code_parts_in_editor(editor):
    '''
    Binds the open file function to ctrl+o shortcut to each code frame in an editor
    '''
    for code_part in editor.code_parts:
        code_part.bind_file_open_shortcut(lambda e: custom_text_ctrl_o_handler(editor))

def unbind_all_code_parts_in_editor(editor):
    '''
    Binds break(unbinds in a way) the ctrl+o shortcut to each code frame in an editor
    '''
    for code_part in editor.code_parts:
        code_part.bind_file_open_shortcut(lambda e: "break")