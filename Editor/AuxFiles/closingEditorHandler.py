import tkinter as tk
from Editor.AuxFiles.basicButtonsFunc import BasicButtonsFunc


class ClosingEditorHandler:
    '''
    Class used for the implementation of closing the editor.
    It asks the user if he/she wants to save the currently opened files
    and performs according to the user's response
    '''

    def __init__(self):
        pass

    def save_and_exit_editor(self, editor):
        '''
        Creates a popup asking if the user wants to save the current tabs and calls the functions doing it
        '''
        save_file_path = editor.editors_path + '/saved_paths.txt'
        if(editor.has_tabs()):
            self.root = tk.Toplevel()
            self.root.grab_set()
            self.root.title('Save open files')
            message_label = tk.Label(self.root, font=editor.code_font,
                                     text='Do you want to save the content of the\n files before closing?')
            message_label.pack(side='top', padx=20, pady=20)
            options_frame = tk.Frame(self.root)
            options_frame.pack(side='top')

            save_button = tk.Button(options_frame, text='Yes',
                                    command=lambda: self.handle_saving_files_before_closing(editor, save_file_path))
            save_button.pack(side='left', padx=10, pady=10)
            dont_save_button = tk.Button(options_frame, text="No",
                                         command=lambda: self.handle_not_saving_files_before_closing(editor,
                                                                                                     save_file_path))
            dont_save_button.pack(side='left', padx=10, pady=10)
            self.root.mainloop()
        else:
            self.save_file_paths(editor,save_file_path)
            editor.root.destroy()


    def handle_saving_files_before_closing(self, editor, save_file_path):
        '''
        Handles the closing of the editor when the content of the files should be saved
        '''
        self.save_file_paths(editor, save_file_path)
        editor.basicButtonFunc.close_and_save_all(editor)
        self.close_pop_up_and_editor(editor)

    def handle_not_saving_files_before_closing(self, editor, save_file_path):
        '''
        Handles the closing of the editor when the content of the files should NOT be saved
        '''
        self.save_file_paths(editor, save_file_path)
        editor.basicButtonFunc.close_all(editor)
        self.close_pop_up_and_editor(editor)

    def save_file_paths(self, editor, save_file_path):
        '''
        Saves the names of the opened files so that they can be reloaded
        '''
        f = open(save_file_path, 'w')
        all_paths = ''
        for path in editor.paths:
            all_paths += path + '\n'
        f.write(all_paths)
        f.close()

    def close_pop_up_and_editor(self, editor):
        '''
        Closes the save popup and the editor itself
        '''
        self.root.destroy()
        editor.root.destroy()
