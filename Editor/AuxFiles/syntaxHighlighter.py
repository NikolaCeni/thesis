class SyntaxHighlighter:
    '''
    Class used for implementing syntax highlighting.
    '''

    def __init__(self):
        pass

    def highlight_syntax(self, editor, code_part=None):
        '''
        Highlighting syntax in a code part
        The code part can be given as parameter or if not,
        syntax highlighting of the current code part is performed
        '''
        for (keywords, tag) in editor.keywords_tags_pairs:
            if (editor.has_tabs()):
                if (code_part == None):
                    code_part = editor.get_current_code_part()
                code_part.tag_remove(tag, '1.0', 'end')
                for keyword in keywords:
                    self.highlight_single_keyword(keyword, tag, code_part)

    def highlight_single_keyword(self, word, tag, code_part):
        '''
        Higlights a specific single keyword word with specific tag
        '''
        code_part.highlight_pattern(word, tag)
