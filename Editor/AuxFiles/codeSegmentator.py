from Editor.AuxFiles.auxFunctions import *
class CodeSegmentator:
    '''
    Class that does code segmentation depending on the selected segmentation strategy
    '''

    def __init__(self):
        self.segments = []
        self.strategies = [0, 1, 2, 3, 4]
        self.strategy = 4

    def separate_segments(self, code_lines):
        '''
        Given a code as list of lines, it creates list of segments depending on the chosen segmentation strategy
        '''
        self.reversed_code_lines = list(reversed(code_lines))
        self.segments.clear()

        while (self.reversed_code_lines != []):
            if (self.strategy == 0):
                self.extract_one_version0()
            elif (self.strategy == 1):
                self.extract_one_version1()
            elif (self.strategy == 2):
                self.extract_one_version2()
            elif (self.strategy == 3):
                self.extract_one_version3()
            elif (self.strategy == 4):
                self.extract_one_version4()
        return self.segments

    def extract_one_version0(self):
        '''
        Strategy 0
        --------------------
        Processes the code bottom to top and it groups all the lines with the same padding as one segment.
        When it comes to the first line with different padding it has two options:
        -if the padding is smaller->Adds that line and stops
        -if the padding is greater->Just stops
        '''
        self.handle_empty_lines()
        i, total_lines = 0, len(self.reversed_code_lines)
        res = ''
        currennt_padd = self.count_padd(i)
        while (i < total_lines and (self.is_comment(self.reversed_code_lines[i]) or self.count_padd(
                i) == currennt_padd and not is_empty_or_new_line(self.reversed_code_lines[i]))):
            line = self.reversed_code_lines[i]
            res = line + '\n' + res
            i += 1
        if (i < total_lines and (self.is_comment(self.reversed_code_lines[i]) or self.count_padd(
                i) < currennt_padd and not is_empty_or_new_line(self.reversed_code_lines[i]))):
            line = self.reversed_code_lines[i]
            res = line + '\n' + res
            i += 1

        self.reversed_code_lines = self.reversed_code_lines[i:]
        self.segments = [res] + self.segments

    def extract_one_version1(self):
        '''
        Strategy 1
        --------------------
        Similar to Strategy 0.
        The only difference is that in case there are more lines one after another having padding
        values which are strictly decreasing, all these lines are considered as part of a single segmen
        '''
        self.handle_empty_lines()
        i, total_lines = 0, len(self.reversed_code_lines)
        res = ''
        currennt_padd = self.count_padd(i)
        while (i < total_lines and (self.is_comment(self.reversed_code_lines[i]) or self.count_padd(
                i) == currennt_padd and not is_empty_or_new_line(self.reversed_code_lines[i]))):
            line = self.reversed_code_lines[i]
            res = line + '\n' + res
            i += 1

        while (i < total_lines and (self.is_comment(self.reversed_code_lines[i]) or self.count_padd(
                i) < currennt_padd and not is_empty_or_new_line(self.reversed_code_lines[i]))):
            currennt_padd = self.count_padd(i)
            line = self.reversed_code_lines[i]
            res = line + '\n' + res
            i += 1
        self.reversed_code_lines = self.reversed_code_lines[i:]
        self.segments = [res] + self.segments

    def extract_one_version2(self):
        '''
        Strategy 2
        --------------------
        Similar to Strategy 1, but empty lines are not considered as segment breaker
        '''
        self.handle_empty_lines()
        i, total_lines = 0, len(self.reversed_code_lines)
        res = ''
        currennt_padd = self.count_padd(i)
        while (i < total_lines and (self.is_comment(self.reversed_code_lines[i]) or self.count_padd(
                i) == currennt_padd or is_empty_or_new_line(self.reversed_code_lines[i]))):
            line = self.reversed_code_lines[i]
            res = line + '\n' + res
            i += 1

        while (i < total_lines and (self.is_comment(self.reversed_code_lines[i]) or self.count_padd(
                i) < currennt_padd or is_empty_or_new_line(self.reversed_code_lines[i]))):
            if not is_empty_or_new_line(self.reversed_code_lines[i]):
                currennt_padd = self.count_padd(i)
            line = self.reversed_code_lines[i]
            res = line + '\n' + res
            i += 1

        self.reversed_code_lines = self.reversed_code_lines[i:]
        self.segments = [res] + self.segments

    def extract_one_version3(self):
        '''
        Strategy 3
        --------------------
        Very similar to strategy 2. The only rule that doesn’t match
        is that  after a line with less padding is found,
        the next line is added if its padding is less or equal
        to the current one(not strictly less as in strategy
        '''
        self.handle_empty_lines()
        i, total_lines = 0, len(self.reversed_code_lines)
        res = ''
        currennt_padd = self.count_padd(i)

        while (i < total_lines and (self.is_comment(self.reversed_code_lines[i]) or self.count_padd(
                i) <= currennt_padd or is_empty_or_new_line(self.reversed_code_lines[i]))):
            if (self.count_padd(i) < currennt_padd):
                currennt_padd = self.count_padd(i)
            line = self.reversed_code_lines[i]
            res = line + '\n' + res
            i += 1
        self.reversed_code_lines = self.reversed_code_lines[i:]
        self.segments = [res] + self.segments

    def extract_one_version4(self):
        '''
        Strategy 4
        --------------------
        Similar to Strategy 3, but empty lines are not considered as segment breaker
        '''
        self.handle_empty_lines()
        i, total_lines = 0, len(self.reversed_code_lines)
        res = ''
        if i < total_lines:
            currennt_padd = self.count_padd(i)
            while (i < total_lines and (self.is_comment(self.reversed_code_lines[i]) or self.count_padd(
                    i) <= currennt_padd) and not is_empty_or_new_line(self.reversed_code_lines[i])):
                if (self.count_padd(i) < currennt_padd):
                    currennt_padd = self.count_padd(i)
                line = self.reversed_code_lines[i]
                res = line + '\n' + res
                i += 1

            self.reversed_code_lines = self.reversed_code_lines[i:]
            self.segments = [res] + self.segments

    def handle_empty_lines(self):
        '''
        Handles the consecutive empty lines and creates a single segment out of them
        '''
        i = 0
        res = ''
        line = self.reversed_code_lines[i]
        total_lines = len(self.reversed_code_lines)
        # handles the empty lines
        if (is_empty_or_new_line(line)):
            while (i < total_lines and is_empty_or_new_line(self.reversed_code_lines[i])):
                res = '\n' + res
                i += 1
        if i > 0:
            self.segments = [res] + self.segments
            self.reversed_code_lines = self.reversed_code_lines[i:]

    def is_comment(self, line):
        '''
        Checks if line starts with '#'-single line comment
        '''
        line_stripped = line.strip()
        if len(line_stripped) > 0:
            return line.strip()[0] == '#'
        return False

    def count_padd(self, line_num):
        '''
        Counts the padding of a given line positioned at line_num in a code
        '''
        line = self.reversed_code_lines[line_num]
        i = 0
        while (i < len(line) and (line[i] == ' ' or line[i] == '\t')):
            if (line[i] == ' '):
                i += 1
            else:
                i += 8
        return i

    def print_segements(self, code):
        '''
        Print segments of given code
        '''
        code_lines = code.split('\n')
        self.separate_segments(code_lines)
        for i in self.segments:
            print("SEGMENT:\n" + i)

    def print_segements_file(self, file):
        '''
        Print segments of given file
        '''
        code_lines = open(file, 'r').readlines()
        self.separate_segments(code_lines)
        for i in self.segments:
            print("SEGMENT:\n" + i)

    def increase_strategy(self):
        '''
        Increment the segmentation strategy by 1
        '''
        self.strategy = ((self.strategy + 1) % len(self.strategies))

    def set_strategy(self, value):
        '''
        Set the segmentation strategy to an explicit value
        '''
        self.strategy = value


def remove_empty_line_padd(code_lines):
    '''
    Removes empty lines which are at the end of a code
    '''
    i = len(code_lines) - 1
    found_non_empty = False
    while i >= 0 and not found_non_empty:
        found_non_empty = not is_empty(code_lines[i])
        i -= 1
    return code_lines[:i]



def is_empty_or_new_line(code):
    '''
    Checks if a given code is full on empty or has only \n or \t
    '''
    for i in code:
        if (i != '' and i != ' ' and i != '\t' and i != '\n'):
            return False
    return True

