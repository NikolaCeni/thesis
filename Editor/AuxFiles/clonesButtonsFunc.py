from Model.model import Model
import random
from Editor.AuxFiles import auxFunctions as aux
import string
from Editor.AuxFiles.singleInputPopup import SingleInputPopup
from Replacer.variablesReplacer import VariablesReplacer
from FunctionGenerator.functionGenerator import FunctionGenerator
from Colors.colors import ColorsGenerator
from Editor.AuxFiles.codeSegmentator import CodeSegmentator


class ClonesButtonsFunc:
    '''
    Class which implements all the features of the editor related to code clones
    '''

    def __init__(self, editor_path):
        self.setting_default_values()
        self.min_threshold = 0.0
        self.max_threshold = 1.0
        self.preprocessor = VariablesReplacer()
        self.color_generator = ColorsGenerator(editor_path)
        self.segmentator = CodeSegmentator()
        self.function_generator = FunctionGenerator(editor_path)
        self.model = Model(editor_path)

    def setting_default_values(self):
        '''
        Setting default values of the parameters
        '''
        self.threshold = 0.95
        self.consider_single_line_segments = True

    def set_seg_strategy(self, editor):
        '''
        Creates popup for explicit setting of the segmentation strategy
        '''
        SingleInputPopup(editor, 'Set strategy', 'Set segmentation strategy', [], condition=0)

    def set_threshold(self, editor):
        '''
         Creates popup for explicit setting of the threshold value
        '''
        SingleInputPopup(editor, 'Set threshold', 'Set similarity threshold',
                         ['Number from 0.0 to 1.0\n1.0 being exact copies\nRecommended value: 0.95'], condition=1)

    def increase_seg_strategy(self, editor):
        '''
        Increments the strategy of code segmentation by 1
        '''
        self.segmentator.increase_strategy()
        editor.change_clones_settings_item(index=1, text='Set Segmentation Strategy (Current: ' + str(
            self.segmentator.strategy) + ')')
        editor.seg_strategy_label['text'] = "Strategy: " + str(self.segmentator.strategy)

    def get_handling_one_line_label_text_color(self):
        '''
        Returns a tuple like (VALUE, COLOR) depending if single line segments are considered or not
        '''
        if (self.consider_single_line_segments):
            return '(On)', 'palegreen'
        else:
            return '(Off)', 'salmon'

    def change_handling_one_line_option(self, editor):
        '''
        Reset the menu item depending if single line segments are considered or not
        '''
        if (self.consider_single_line_segments):
            self.consider_single_line_segments = False
            editor.change_clones_settings_item(index=0, text='Consider One Line Segments (Off)',
                                               background='salmon')
        else:
            self.consider_single_line_segments = True
            editor.change_clones_settings_item(index=0, text='Consider One Line Segments (On)',
                                               background='palegreen')

    def suggest_functions(self, editor):
        '''
        Creates suggested functions and prints them in the output part
        '''
        if (editor.has_tabs()):
            self.find_clones(editor)
            if len(editor.cloned_pairs) == 0:
                all_functions = 'No cloned pairs found!'
            else:
                all_functions = 'SUGGESTED FUNCTIONS:\n\n'
                self.function_generator.reset_counters()
                for (first, second) in editor.cloned_pairs:
                    first_seg, sec_seg = editor.segments[first], editor.segments[second]
                    all_functions += self.function_generator.generate_function_given_codes(first_seg, sec_seg) + '\n'
            editor.set_output(all_functions)

    def find_clones(self, editor):
        '''
        Resets the segments
        Does pairwise comparison and handles cloned segments
        '''
        if (editor.has_tabs()):

            code_part = editor.get_current_code_part()
            code = code_part.get('1.0', 'end-1c')
            code_lines = code.split('\n')
            for tag in code_part.tag_names():
                if (tag not in ['keywords', "loop_and_logical_keywords", 'logical_values', 'class_keywords',
                                "comments_keywords"]):
                    code_part.tag_delete(tag)
            editor.segments = self.segmentator.separate_segments(code_lines)
            self.tag_color_per_segment = [[None, None] for _ in editor.segments]
            editor.segments_number = len(editor.segments)
            self.color_generator.reset()
            editor.cloned_pairs.clear()
            self.preprocessor.reset_counters()
            for first in range(editor.segments_number):
                first_seg = editor.segments[first]
                random_string = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))
                for second in range(first):
                    sec_seg = editor.segments[second]
                    if (not self.skip_segment(editor.segments[second])):
                        (are_clones, predicted_distance) = self.model.predict(first_seg, sec_seg, self.threshold,
                                                                              self.preprocessor)
                        # (are_clones, predicted_distance) = (False, 1)
                        if are_clones:
                            self.handle_cloned_segments(editor, code_part, first, second, random_string)

    def handle_cloned_segments(self, editor, code_part, first, second, random_string):
        '''
        Handles the highlighting of cloned segments
        '''
        already_cloned_segments = [index for tuple in editor.cloned_pairs for index in tuple]
        if (first not in already_cloned_segments and second not in already_cloned_segments):
            editor.cloned_pairs.append((first, second))
        first_code_start = self.calculate_start_row(editor.segments, first)
        first_code_len = self.calculate_code_len(editor.segments, first)
        sec_code_start = self.calculate_start_row(editor.segments, second)
        sec_code_len = self.calculate_code_len(editor.segments, second)
        editor.hasTag = True
        if (self.tag_color_per_segment[second][0] == None and self.tag_color_per_segment[first][0] == None):
            color = self.color_generator.generate_color()
            code_part.tag_add('color_' + random_string, str(float(first_code_start + 1)),
                              str(first_code_start + first_code_len) + ".end")
            self.tag_color_per_segment[first][0] = 'color_' + random_string
            self.tag_color_per_segment[first][1] = color
            code_part.tag_add('color_' + random_string, str(float(sec_code_start + 1)),
                              str(sec_code_start + sec_code_len) + ".end")
            self.tag_color_per_segment[second][0] = 'color_' + random_string
            self.tag_color_per_segment[second][1] = color
            code_part.tag_configure('color_' + random_string,
                                    background=color)
        elif self.tag_color_per_segment[second][0] != None and self.tag_color_per_segment[first][0] == None:
            tag = self.tag_color_per_segment[second][0]
            code_part.tag_add(tag, str(float(first_code_start + 1)),
                              str(first_code_start + first_code_len) + ".end")
            self.tag_color_per_segment[first][0] = tag
            self.tag_color_per_segment[first][1] = self.tag_color_per_segment[second][1]
        elif self.tag_color_per_segment[second][0] == None and self.tag_color_per_segment[first][0] != None:
            tag = self.tag_color_per_segment[first][0]
            code_part.tag_add(tag, str(float(sec_code_start + 1)),
                              str(sec_code_start + sec_code_len) + ".end")
            self.tag_color_per_segment[second][0] = tag
            self.tag_color_per_segment[second][1] = self.tag_color_per_segment[first][1]
        else:
            tag_first = self.tag_color_per_segment[first][0]
            tag_second = self.tag_color_per_segment[second][0]
            if (tag_second != tag_first):
                color_first = self.tag_color_per_segment[first][1]
                code_part.tag_configure(tag_second, background=color_first)

    def skip_segment(self, segment):
        '''
        Reeturns True or False depending if the segment should be considered or not
        '''
        return self.consider_single_line_segments == False and aux.is_one_line(segment)

    def calculate_code_len(self, segments, index):
        '''
        Calculate the length of the segment with given index
        '''
        code = segments[index]
        return len(code.split('\n')) - 1

    def calculate_start_row(self, segments, index):
        '''
        Calculate the starting row of the segment with given index
        '''
        return sum([self.calculate_code_len(segments, i) for i in range(index)])

    def concat_lines(self, lines):
        '''
        Concatinates a list of lines into a single code
        '''
        res = ""
        for i in lines[:-1]:
            res += i + '\n'
        return res + lines[-1]
