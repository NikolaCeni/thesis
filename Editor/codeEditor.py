import os
import tkinter as tk
import tkinter.font as font
from Editor.AuxFiles import auxFunctions as aux
from tkinter import ttk
from win32api import GetSystemMetrics
from Editor.AuxFiles.customText import CustomText
from PIL import ImageTk, Image
from Editor.AuxFiles.basicButtonsFunc import BasicButtonsFunc
from Editor.AuxFiles.clonesButtonsFunc import ClonesButtonsFunc
from Editor.AuxFiles.closingEditorHandler import ClosingEditorHandler
from Editor.AuxFiles.syntaxHighlighter import SyntaxHighlighter
import _thread


class CodeEditor:
    def __init__(self):
        self.editors_path = os.getcwd() + '\\'
        self.height = GetSystemMetrics(1)
        self.width = GetSystemMetrics(0)

        self.color = 'lightblue'
        self.header_footer_color = 'lightslategrey'
        self.code_bg_color = 'dimgray'
        self.cursor_color = 'white'
        self.code_color = 'white'
        self.output_text = 'black'
        self.output_bg = 'white'
        self.panned_window_bg_color = 'grey80'
        self.basicButtonFunc = BasicButtonsFunc()
        self.clonesButtonFunc = ClonesButtonsFunc(self.editors_path)
        self.closeEditorHandler = ClosingEditorHandler()
        self.syntaxHighlighter = SyntaxHighlighter()
        self.root = tk.Tk()
        self.root.geometry("1300x700")
        self.root.protocol("WM_DELETE_WINDOW", lambda: self.closeEditorHandler.save_and_exit_editor(self))
        self.root.config(width=self.width, height=self.height, bg=self.color)
        self.has_tag = False
        self.has_output_frame = False
        self.all_tag_names = []
        self.tags_for_clones = []
        self.all_words = []
        self.paths = []
        self.code_parts = []
        self.tab_frames = []
        self.segments = []
        self.cloned_pairs = []
        self.file_extensions = ['py', 'txt', 'csv']
        self.code_font = font.Font(family='Monoid')
        self.set_editor_logo_and_name()
        self.handle_keywords()
        self.create_menu_bar()
        self.create_header()
        self.create_footer()
        self.fill_header_with_buttons()
        self.general_paned_window = tk.PanedWindow(bg=self.panned_window_bg_color, orient=tk.VERTICAL, borderwidth=10,
                                                   sashwidth=30)
        self.general_paned_window.pack(side='top', expand=True, fill='both')
        self.code_paned_window = tk.PanedWindow(self.general_paned_window, bg=self.header_footer_color,
                                                orient=tk.VERTICAL, borderwidth=5)
        self.general_paned_window.add(self.code_paned_window)
        self.insert_code_part()
        self.create_output_part()
        self.bind_shortcuts()
        self.root.bind('<Key>', lambda x: self.basicButtonFunc.handle_key_pressed(self))
        self.create_saved_tabs()
        self.root.mainloop()

    def handle_undo(self, shortcut=True):
        '''
        Event handler for Ctrl+Z
        '''
        if self.has_tabs():
            code_part = self.get_current_code_part()
            code_part.undo_content(shortcut=shortcut)
            self.syntaxHighlighter.highlight_syntax(self)

    def set_editor_logo_and_name(self):
        '''
        Sets the name and the logo of the Editor
        '''
        self.root.title("Python Code Editor")
        image_file = self.editors_path + r'Editor\Icons\logo.png'
        img = Image.open(image_file)
        img.save(self.editors_path + 'Editor/Icons/logo.ico', format='ICO', sizes=[(32, 32)])
        self.root.iconbitmap(self.editors_path + 'Editor/Icons/logo.ico')

    def handle_keywords(self):
        '''
        Creates lists of python keywords and creates tags for each group of keywords
        '''
        self.keywords = ['None', 'in ', 'is ', 'def ', 'except ', 'finally',
                         'from ', 'global ', 'import ', 'lambda', 'return', 'try', 'print', '(', ')', '[', ']', '{',
                         '}']
        self.keywords_color = 'aquamarine'

        self.loop_and_logical_keywords = ['and ', 'or ', 'not ', 'if', 'elif', 'else', 'while ', 'for ', 'break',
                                          'continue']
        self.loop_and_logical_keywords_color = 'limegreen'

        self.logical_values = ['False ', 'True ']
        self.logical_values_color = 'gold'

        self.class_keywords = ['class', 'self']
        self.class_keywords_color = 'skyblue'
        self.comments_keywords = ['#', '\'\'\'']
        self.comments_keywords_color = 'grey60'
        self.keywords_tags_pairs = [(self.logical_values, "logical_values"), (self.keywords, "keywords"),
                                    (self.loop_and_logical_keywords, "loop_and_logical_keywords"),
                                    (self.class_keywords, "class_keywords"),
                                    (self.comments_keywords, "comments_keywords")]

    def configure_tags(self, code_part):
        '''
        Configures the tags used for syntax highlighting
        '''
        code_part.tag_configure("keywords", foreground=self.keywords_color)
        code_part.tag_configure("loop_and_logical_keywords", foreground=self.loop_and_logical_keywords_color)
        code_part.tag_configure("logical_values", foreground=self.logical_values_color)
        code_part.tag_configure("class_keywords", foreground=self.class_keywords_color)
        code_part.tag_configure("comments_keywords", foreground=self.comments_keywords_color)

    def create_menu_bar(self):
        '''
        Creates the menu bar and the menu items
        '''
        menu_bar = tk.Menu(self.root)
        self.file_item = tk.Menu(menu_bar, tearoff=0, activeborderwidth=4)
        self.file_item.add_command(label='New..        Ctrl+N', command=lambda: self.basicButtonFunc.add_file(self))
        self.file_item.add_command(label='Open         Ctrl+O', command=lambda: self.basicButtonFunc.open_file(self))
        self.file_item.add_command(label='Save         Ctrl+S', command=lambda: self.basicButtonFunc.save(self))
        self.file_item.add_command(label='Close', command=lambda: self.basicButtonFunc.handle_closing_file(self))
        self.file_item.add_command(label='Close All', command=lambda: self.basicButtonFunc.close_all(self))
        self.file_item.add_command(label='Close and Save All',
                                   command=lambda: self.basicButtonFunc.close_and_save_all(self))
        menu_bar.add_cascade(label='File', menu=self.file_item)

        self.edit_item = tk.Menu(menu_bar, tearoff=0, activeborderwidth=4)
        self.edit_item.add_command(label='Change Theme', command=lambda: self.basicButtonFunc.change_theme(self))
        self.edit_item.add_command(label='Clear Code', command=lambda: self.basicButtonFunc.clear_code_window(self))
        self.edit_item.add_command(label='Undo Code   Ctrl+Z',
                                   command=lambda: self.basicButtonFunc.handle_undo(self, shortcut=False))
        self.edit_item.add_command(label='Refresh Tab',
                                   command=lambda: self.basicButtonFunc.refresh_tab(self))
        self.edit_item.add_command(label='Refresh All', command=lambda: self.basicButtonFunc.refresh_tabs(self))
        menu_bar.add_cascade(label='Edit', menu=self.edit_item)

        self.view_item = tk.Menu(menu_bar, tearoff=0, activeborderwidth=4)
        self.view_item.add_command(label='Shortcuts        ', command=aux.list_shortcuts)
        self.view_item.add_command(label='User Manual      ', command=aux.list_information)
        menu_bar.add_cascade(label='View', menu=self.view_item)

        self.run_item = tk.Menu(menu_bar, tearoff=0, activeborderwidth=4)
        self.run_item.add_command(label='Run              Ctrl+R',
                                  command=lambda: self.basicButtonFunc.save_and_run(self))
        menu_bar.add_cascade(label='Run', menu=self.run_item)

        self.clones_set_item = tk.Menu(menu_bar, tearoff=0, activeborderwidth=4)
        (on_off, color) = self.clonesButtonFunc.get_handling_one_line_label_text_color()
        self.clones_set_item.add_command(label='Consider One Line Segments ' + on_off, background=color,
                                         command=lambda: self.clonesButtonFunc.change_handling_one_line_option(self))
        self.clones_set_item.add_command(
            label='Set Segmentation Strategy (Current: ' + str(self.clonesButtonFunc.segmentator.strategy) + ')',
            command=lambda: self.clonesButtonFunc.set_seg_strategy(self))
        self.clones_set_item.add_command(
            label='Set Similarity Threshold (Current: ' + str(self.clonesButtonFunc.threshold) + ')',
            command=lambda: self.clonesButtonFunc.set_threshold(self))
        menu_bar.add_cascade(label='Clones Settings', menu=self.clones_set_item)

        self.root.config(menu=menu_bar)

    def create_header(self):
        '''
        Creates the header that contains the main buttons
        '''
        self.header = tk.Frame(self.root, height=50, bg=self.header_footer_color, borderwidth=1, relief="solid")
        self.header.pack(side='top', fill='x')

    def create_footer(self):
        '''
        Creates the footer containing the seg strategy and button meaning label
        '''
        self.footer = tk.Frame(self.root, height=50, bg=self.header_footer_color, borderwidth=1, relief="solid")
        self.footer.pack(side='bottom', fill='x')

        self.seg_strategy_label = tk.Label(self.footer, bg=self.header_footer_color,
                                           text="Strategy: " + str(self.clonesButtonFunc.segmentator.strategy),
                                           width=15,
                                           font=('Monoid', 14, 'bold'))
        self.seg_strategy_label.pack(side='right')

        self.button_meaning_label = tk.Label(self.footer, bg=self.header_footer_color, width=25,
                                             font=('Monoid', 14, 'bold'))
        self.button_meaning_label.pack(side='right', padx=20)

    def fill_header_with_buttons(self):
        '''
        Fills the header with buttons
        '''
        self.add_buton(self.header, 'Close File', lambda: self.basicButtonFunc.handle_closing_file(self),
                       self.editors_path + "Editor/Icons/close.png")
        self.add_buton(self.header, 'Suggest Functions', lambda: self.clonesButtonFunc.suggest_functions(self),
                       self.editors_path + "Editor/Icons/suggest.png")
        self.add_buton(self.header, 'Increase Segmentation Strategy',
                       lambda: self.clonesButtonFunc.increase_seg_strategy(self),
                       self.editors_path + "Editor/Icons/change_strategy.png")
        self.add_buton(self.header, 'Detect Clones', self.start_detecting_clones,
                       self.editors_path + "Editor/Icons/clone.ico")
        self.add_buton(self.header, 'Open File', lambda: self.basicButtonFunc.open_file(self),
                       self.editors_path + "Editor/Icons/open.png", side='left')
        self.add_buton(self.header, 'Add File', lambda: self.basicButtonFunc.add_file(self),
                       self.editors_path + "Editor/Icons/add.png", side='left')
        self.add_buton(self.header, 'Save Code', lambda: self.basicButtonFunc.save(self),
                       self.editors_path + "Editor/Icons/save.png", side='left')
        self.add_buton(self.header, 'Run Code', lambda: self.basicButtonFunc.save_and_run(self),
                       self.editors_path + "Editor/Icons/run.png", side='left')

        self.button_meaning_label.pack(side='right')
        self.seg_strategy_label.pack(side='right', padx=20)

    def bind_shortcuts(self):
        '''
        Defines the shortcuts that can be used in the editor
        '''
        self.root.bind('<Control-Key-x>', lambda e: self.basicButtonFunc.handle_closing_file(self))
        self.root.bind('<Control-Key-X>', lambda e: self.basicButtonFunc.handle_closing_file(self))
        self.root.bind('<Control-Key-r>', lambda e: self.basicButtonFunc.save_and_run(self))
        self.root.bind('<Control-Key-R>', lambda e: self.basicButtonFunc.save_and_run(self))
        self.root.bind('<Control-Key-s>', lambda e: self.basicButtonFunc.save(self))
        self.root.bind('<Control-Key-S>', lambda e: self.basicButtonFunc.save(self))
        self.root.bind('<Control-Key-o>', lambda e: self.basicButtonFunc.open_file(self))
        self.root.bind('<Control-Key-O>', lambda e: self.basicButtonFunc.open_file(self))
        self.root.bind('<Control-Key-n>', lambda e: self.basicButtonFunc.add_file(self))
        self.root.bind('<Control-Key-N>', lambda e: self.basicButtonFunc.add_file(self))
        self.root.bind('<Control-Key-z>', lambda e: self.handle_undo(shortcut=True))
        self.root.bind('<Control-Key-Z>', lambda e: self.handle_undo(shortcut=True))
        self.root.bind('<Control-Key-1>', lambda e: self.start_detecting_clones())
        self.root.bind('<Control-Key-2>', lambda e: self.clonesButtonFunc.increase_seg_strategy(self))
        self.root.bind('<Control-Key-3>', lambda e: self.clonesButtonFunc.suggest_functions(self))

    def unbind_shortcuts(self):
        '''
        Unbinds the shortcuts used in the editor, used when there is a window open
        '''
        self.root.unbind('<Control-Key-x>')
        self.root.unbind('<Control-Key-X>')
        self.root.unbind('<Control-Key-r>')
        self.root.unbind('<Control-Key-R>')
        self.root.unbind('<Control-Key-s>')
        self.root.unbind('<Control-Key-S>')
        self.root.unbind('<Control-Key-o>')
        self.root.unbind('<Control-Key-O>')
        self.root.unbind('<Control-Key-n>')
        self.root.unbind('<Control-Key-N>')
        self.root.unbind('<Control-Key-z>')
        self.root.unbind('<Control-Key-Z>')
        self.root.unbind('<Control-Key-1>')
        self.root.unbind('<Control-Key-2>')
        self.root.unbind('<Control-Key-3>')

    def start_detecting_clones(self):
        '''
        Creates a thread that will strat detecting clones
        '''
        _thread.start_new_thread(self.clonesButtonFunc.find_clones, (self,))

    def add_buton(self, frame, text, command, image_path, side='right'):
        '''
        Adds a button with a specific text to a specific frame in the specific side
        '''
        button_dimension = (30, 30)
        button_image = ImageTk.PhotoImage(
            Image.open(image_path).resize(button_dimension, Image.ANTIALIAS))
        button = tk.Button(frame, text=text, image=button_image, command=command)
        button.image = button_image
        button.pack(side=side, padx=10, pady=5)
        button.bind('<Enter>', lambda e: self.set_meaning_label(text))
        button.bind('<Leave>', lambda e: self.set_meaning_label(''))

    def set_meaning_label(self, text):
        '''
        Resets the text in the label explaining the functions of a button
        '''
        self.button_meaning_label['text'] = text

    def insert_code_part(self):
        '''
        Creating the code block in the GUI
        '''
        self.code_frame = tk.Frame(self.code_paned_window)
        self.code_paned_window.add(self.code_frame)
        self.create_home_page()
        self.tab_control = ttk.Notebook(self.code_frame)

    def create_home_page(self):
        '''
        Creates home page with general info
        '''
        home_page_text = open(self.editors_path + 'Editor/AuxFiles/home_page.txt', 'r').read()
        welcome_text = "Welcome to Python code editor"
        self.welcome_label = tk.Label(self.code_frame, text=welcome_text,
                                      font=font.Font(family='Monoid', size=35, weight='bold'), bg=self.color, height=7,
                                      width=20, justify='left')
        self.welcome_label.pack(side='top', expand=True, fill='both')
        self.home_label = tk.Label(self.code_frame, text=home_page_text,
                                   font=font.Font(family='Monoid', size=20, weight='bold'), bg=self.color, height=7,
                                   width=20, justify='left')
        self.home_label.pack(side='top', expand=True, fill='both')

    def create_tab(self, file_path):
        '''
        Creates a single code text widget based on a file name
        '''
        file_name = file_path.split('/')[-1]
        if (len(self.tab_control.tabs()) == 0):
            self.home_label.pack_forget()
            self.welcome_label.pack_forget()
        tab = tk.Frame(self.tab_control, bg=self.color)

        self.tab_control.add(tab, text=file_name)
        self.tab_control.pack(expand=True, fill="both")

        code_part = CustomText(tab, insertbackground=self.cursor_color, bg=self.code_bg_color, fg=self.code_color,
                               font=self.code_font)
        code_part.bind_file_open_shortcut(lambda e: aux.custom_text_ctrl_o_handler(self))
        code_part.pack(side='left', expand=True, fill='both')

        scrollb = tk.Scrollbar(tab, command=code_part.yview)
        scrollb.pack(side='right', fill='y')
        code_part['yscrollcommand'] = scrollb.set
        self.configure_tags(code_part)
        self.code_parts.append(code_part)
        self.tab_frames.append(tab)
        self.paths.append(file_path)

        return code_part

    def create_output_part(self):
        '''
        Creates the output part
        '''
        self.output_frame = tk.Frame(self.general_paned_window, height=600)
        self.output_buttons_frame = tk.Frame(self.output_frame, bg=self.header_footer_color, height=50, borderwidth=1,
                                             relief="solid")
        self.output_buttons_frame.pack(side='top', expand=True, fill='x')
        self.output_label = tk.Label(self.output_buttons_frame, bg=self.header_footer_color,
                                     text="Output:", width=10,
                                     font=('Monoid', 14, 'bold'))
        self.output_label.pack(side='left')
        self.add_buton(self.output_buttons_frame, 'Close Output', self.hide_output_frame,
                       self.editors_path + "Editor/Icons/close.png")
        self.add_buton(self.output_buttons_frame, 'Clear Ouput', lambda: self.set_output(''),
                       self.editors_path + "Editor/Icons/clear_output.png")
        self.output_part = tk.Text(self.output_frame, state=tk.DISABLED, font=self.code_font,
                                   foreground=self.output_text,
                                   bg=self.output_bg, height=50)
        self.output_part.tag_configure("error", foreground='red')
        self.output_part.pack(side='left', expand=True, fill='both')
        scrollb = tk.Scrollbar(self.output_frame, command=self.output_part.yview)
        scrollb.pack(side='right', fill='y')
        self.output_part['yscrollcommand'] = scrollb.set

    def show_output_frame(self):
        '''
        Shows(packs) the output part
        '''
        self.general_paned_window.add(self.output_frame)
        self.has_output_frame = True

    def hide_output_frame(self):
        '''
        Hides (pack forget) the output part
        '''
        self.general_paned_window.forget(self.output_frame)
        self.has_output_frame = False

    def set_output(self, output, error=False):
        '''
        Set output text in the output label
        If the error is True then the text color is set to red
        '''
        if (not self.has_output_frame):
            self.show_output_frame()
        self.output_part.config(state=tk.NORMAL)
        self.output_part.delete(1.0, "end")
        self.output_part.insert(1.0, output)
        self.output_part.config(state=tk.DISABLED)
        if (error):
            self.output_part.tag_add("error", 1.0, 'end')
        else:
            self.output_part.tag_remove("error", 1.0, 'end')

    def create_saved_tabs(self):
        '''
        Loads the saved files
        '''
        f = open(self.editors_path + '/saved_paths.txt')
        for path in f.readlines():
            self.basicButtonFunc.open_file(self, path[:-1])

    def has_tabs(self):
        '''
        Checks if there is at least one tab in the editor not includin the home page
        '''
        return len(self.tab_control.tabs()) > 0

    def get_current_code_part(self):
        '''
        Returns the code frame(Text widget) placed in the current tab
        '''
        return self.code_parts[self.tab_control.index('current')]

    def change_clones_settings_item(self, index, text, background=None, foreground=None):
        '''
        Changes the "Clones settings" menu item at given index
        '''
        self.clones_set_item.entryconfig(index, label=text, background=background, foreground=foreground)

    def update_strategy_label(self):
        '''
        Updates the "Segmentation strategy: X" label based on the current strategy
        '''
        self.seg_strategy_label.config(text="Strategy: " + str(self.clonesButtonFunc.segmentator.strategy))
