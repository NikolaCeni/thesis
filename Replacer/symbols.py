'''
Modules which contains some of the most used python operators, symbols and delimeters
'''
functions = ['print', 'range']
operators = ['!', '+', '-', '/', '//', '&', '^', '~', '|', '**', '<<', '>>', '<', '>', '%', '*', '@', '==', '!=', '>=',
             '<=', '=']
delimeters = [',', '#', '\'\'\'', ',', '.', '\'', '"', ':', '\\']
brackets = ['[', ']', '{', '}', '(', ')']

def isfunction(word):
    '''
    Checks if a given word is a function
    '''
    return word in functions

def isoperator(word):
    '''
    Checks if a given word is an operator
    '''
    return word in operators


def isdemiliter(word):
    '''
    Checks if a given word is a delimiter
    '''
    return word in delimeters


def isbracket(word):
    '''
    Checks if a given word is a bracket
    '''
    return word in brackets


def issymbol(word):
    '''
    Checks if a given word is a symbol
    '''
    return isdemiliter(word) or isoperator(word) or isbracket(word)
