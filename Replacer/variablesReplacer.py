import html
from torchtext.data.utils import get_tokenizer
from keyword import *
from Replacer.symbols import *


class VariablesReplacer:
    '''
    Class used for renaming functions, classes, comments and variables into fun_, class_,comment_, var_
    where _ is an integer number
    '''

    def __init__(self):
        self.function_names = {}
        self.class_names = {}
        self.comm_names = {}
        self.var_names = {}
        self.reset_counters()
        self.module = 200

    def reset_counters(self):
        '''
        Resets the counter which basically represent the next number that is going to be used for naming
        '''
        self.func_cnt = 0
        self.class_cnt = 0
        self.comm_cnt = 0
        self.var_cnt = 0

    def preprocess_single_code(self, code):
        '''
        It preprocess a single code and returns tokenized version of it
        '''
        code = self.remove_function_signature(code)
        code = self.remove_multiline_comments(code)
        code = code.replace('_', 'a')  # in order to tokenize abc_def as one token
        code = code.replace('\n', '_')  # in order to keep track of the new lines
        tokenize = get_tokenizer("moses")
        code_tokenized = tokenize(code)
        code_tokenized = self.replace(code_tokenized)
        return code_tokenized

    def remove_multiline_comments(self, code):
        '''
        Removes multiple line comments of a given code
        '''
        s = code.split("'''")
        res = ''
        if len(s) > 0:
            i = 0
            while i < len(s):
                code = s[i]
                if (len(code) > 0 and not isoperator(code[-1])):
                    res += code
                i += 2
        return res

    def remove_function_signature(self, code):
        '''
        If the code passed is a function it removes its function signature
        '''
        try:
            first_line = code.split('\n')[0]
            if ('def ' in first_line):
                len_first_line = len(first_line) + 1  # +1 because pf the \n in the end
                code = code[len_first_line:]
        except:
            pass
        return code

    def replace(self, code_tokenized):
        '''
        Does the functions, classes, comments and variables replacing
        '''
        code_tokenized = self.replace_functions(code_tokenized)

        code_tokenized = self.replace_classes(code_tokenized)

        code_tokenized = self.replace_variables(code_tokenized)

        code_tokenized = self.replace_comments(code_tokenized)
        return code_tokenized

    def replace_functions(self, code_tokenized):
        '''
        Replaces functions
        '''
        self.function_names = {}
        for (ind, token) in enumerate(code_tokenized):
            token = html.unescape(token)
            if (not iskeyword(token) and not issymbol(token)):
                if token in self.function_names:
                    code_tokenized[ind] = self.function_names[token]
                else:
                    if token == 'def':
                        replacer_word = 'fun' + str(self.func_cnt)
                        self.function_names[code_tokenized[ind + 1]] = replacer_word
                        self.func_cnt = (self.func_cnt + 1) % self.module
                        # self.func_cnt = (self.func_cnt + 1)
                try:
                    if code_tokenized[ind + 1] == '(':
                        replacer_word = 'fun' + str(self.func_cnt)
                        self.function_names[code_tokenized[ind]] = replacer_word
                        code_tokenized[ind] = replacer_word
                        self.func_cnt = (self.func_cnt + 1) % self.module
                #       self.func_cnt = (self.func_cnt + 1)
                except:
                    pass  # when there is not ind+1

        return code_tokenized

    def replace_classes(self, code_tokenized):
        '''
        Replaces classes
        '''
        #  self.class_cnt = 0
        self.class_names = {}
        for (ind, token) in enumerate(code_tokenized):
            token = html.unescape(token)
            if (not iskeyword(token) and not issymbol(token) and token not in self.function_names):
                if token in self.class_names:
                    code_tokenized[ind] = self.class_names[token]
                if token == 'class':
                    replacer_word = 'class' + str(self.class_cnt)
                    self.class_names[code_tokenized[ind + 1]] = replacer_word
                    self.class_cnt = (self.class_cnt + 1) % self.module
                # self.class_cnt = (self.class_cnt + 1)

        return code_tokenized

    def replace_comments(self, code_tokenized):
        '''
        Replaces comments
        '''
        self.comm_names.clear()
        #  self.comm_cnt = 0
        ind = 0
        while ind < len(code_tokenized):
            token = html.unescape(code_tokenized[ind])
            if (token == '#'):
                code_tokenized[ind + 1] = 'comment' + str(self.comm_cnt)
                # self.comm_cnt+=1
                # self.comm_cnt=(self.comm_cnt+1)%self.module
                ind += 2
                token = html.unescape(code_tokenized[ind])
                while token != '_' and ind < len(code_tokenized):
                    code_tokenized.pop(ind)
                    if ind == len(code_tokenized):
                        break
                    token = code_tokenized[ind]
                    ind += 1
            ind += 1
        return code_tokenized

    def replace_variables(self, code_tokenized):
        '''
        Replaces variables
        '''
        # self.var_cnt = 0
        self.var_names.clear()
        for (ind, token) in enumerate(code_tokenized):
            token = html.unescape(token)
            if (not (isfunction(token) or iskeyword(token) or issymbol(token) or self.isfunction(token) or self.isclass(
                    token) or token == '_' or self.isnumeric(token))):
                if token in self.var_names:
                    code_tokenized[ind] = self.var_names[token]
                else:
                    replacer_word = 'var' + str(self.var_cnt)
                    self.var_names[token] = replacer_word
                    code_tokenized[ind] = replacer_word
                    self.var_cnt = (self.var_cnt + 1) % self.module
                    # self.var_cnt+=1

        return code_tokenized

    def isfunction(self, word):
        '''
        Checks if there was a function with given name already detected
        '''
        return word in list(self.function_names.values())

    def isclass(self, word):
        '''
        Checks if there was a class with given name already detected
        '''
        return word in list(self.class_names.values())

    def isnumeric(self, word):
        '''
        Checks if a string is numeric value(float or int)
        '''
        try:
            temp = float(word)
            return True
        except:
            return False

    def untokenize_and_print(self, code_tokenized):
        '''
        Joings a list of tokens into a single string and prints it
        '''
        code = self.put_together(code_tokenized)
        print(code)

    def put_together(self, code_tokenized):
        '''
        Joins a list of tokens into a single string
        :param code_tokenized:
        :return:
        '''
        code = ''
        for i in code_tokenized:
            if (i == '_'):  # denotes new line
                code += '\n'
            else:
                code += html.unescape(i) + ' '
        return code
