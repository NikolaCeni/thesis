import random


class ColorsGenerator:
    '''
    Class that reads colors from a file, shuffles them and returns
    a color based on a counter
    '''

    def __init__(self,path):
        f = open(path+'Colors\\colors.txt', 'r')
        self.colors = f.readlines()
        #random.shuffle(self.colors)
        self.counter = 0
        self.total_colors = len(self.colors)
        f.close()
        for (ind, color) in enumerate(self.colors):
            self.colors[ind] = color[:-1]

    def reset(self):
        '''
        Resets the counter
        '''
        self.counter = 0

    def generate_color(self):
        '''
        Gives back the next color depending on the counter
        '''
        if (self.counter < self.total_colors):
            self.counter += 1
            return self.colors[self.counter - 1]
        else:
            return self.get_random_color()

    def get_random_color(self):
        '''
        Returns a random color(used if the counter exceeds the colors in the file
        '''
        color = "#" + ''.join([random.choice('0123456789ABC') for j in range(6)])
        return color
