# README #

The goal of the project is creating Python code editor capable of code clone detection

### How do I get set up? ###
All the required libraries and their version are listed in the requirements.txt file.
They can be installed by running the following 2 commands:

FOR /F %k in (requirements.txt) DO pip install %k
pip install torchtext